{% if output.name != "ebook" %}

# General overview

{% endif %}

FatScript is a lightweight, interpreted programming language designed for building console-based applications. It emphasizes simplicity, ease of use, and functional programming concepts.

## But, wait...

> **Fat**Script, a **lightweight** programming language?

Yes, there's something odd about that statement... but please let me explain.

The expression "syntactic sugar" refers to features that make code easier to write by hiding underlying complexity. And, as with consuming too much sugar... there can be consequences: fatness. Which, in this sense, it's a good thing - a lot of weight in few lines of code.

That said, FatScript is still a relatively new language, and although it's designed to be simple and intuitive, it may not be the best fit for all tasks, especially when it comes to high-performance computing or extremely complex workloads. However, despite its name, FatScript's interpreter is tiny (lightweight), with a near-zero startup cost and [benchmarking](https://www.youtube.com/watch?v=EH7FtBdb3pE) shows it performing comparably to languages like Python or JavaScript.

So while calling it "lightweight" might be debatable, the language runtime is not inherently bloated and maintains an efficient profile in practice for most use cases.

## Key Concepts

- Automatic memory management through garbage collection (GC)
- Symbolic character combinations for a minimalistic syntax
- REPL (Read-Eval-Print Loop) for quick expression testing
- Support for type system, inheritance, and sub-typing via aliases
- Support for immutable programming and passable methods (as values)
- Keep it simple and intuitive, whenever possible

## Free and open-source

`fatscript/fry` is an open-source project that encourages knowledge sharing and collaboration. We welcome developers to [contribute](https://gitlab.com/fatscript/fry/blob/main/CONTRIBUTING.md) to the project and help us improve it over time.

## Contents of this section

- [Setup](setup.md): how to install the FatScript interpreter
- [Options](options.md): how to customize the runtime
- [Bundling](bundling.md): how to pack a FatScript application
- [Tooling](tooling.md): overview of a few extra tools and resources
