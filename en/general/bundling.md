{% if output.name != "ebook" %}

# Bundling

{% endif %}

Fry offers an integrated bundling tool for FatScript code.

## Usage

To bundle your project into a single file starting from the entry point, execute:

```
fry -b sweet mySweetProject.fat
```

Subsequently, you can run your program:

```
./sweet
```

This process does the following:

- Consolidates all imports, except for standard libraries and [literal paths](../syntax/imports.md#literal-paths)
- Removes spaces and comments to enhance load times
- Replaces any `$break` statements (debugger breakpoint) with `()`
- Adds a [shebang](https://bash.cyberciti.biz/guide/Shebang) to bundled code
- Receives the execute attribute for file mode

## Caveats

Imports are deduplicated and inlined based on their order of first appearance. As a result, the sequence in which you import your files could play a role in the final bundled output. Though these considerations are usually inconsequential for small projects, bundling larger projects may require additional organization. Always validate your bundled output.

## Obfuscating

For optional obfuscation, use `-o`:

```
fry -o sweet mySweetProject.fat  # creates the obfuscated bundle
./sweet                          # executes your program as usual
```

> when distributing via public hosts, consider [setting a custom key](../libraries/sdk.md#setkey) with a local `.fryrc`; Only the client should be privy to this key to safeguard the source

Obfuscation leverages [enigma](../libraries/enigma.md) algorithm for encryption, ensuring swift decoding. For optimal load times, prefer `-b` if obfuscation isn't essential.
