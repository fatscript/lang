{% if output.name != "ebook" %}

# Setup

{% endif %}

To start "frying" your fat code, you'll need an interpreter for the FatScript programming language.

## fry, The FatScript Interpreter

[fry](https://gitlab.com/fatscript/fry) is a free interpreter and runtime environment for FatScript. You can install it on your machine using the following instructions.

## Installation

`fry` is designed for GNU/Linux, but it might also work on [other operating systems](#os-support).

For Arch-based distributions, install via [fatscript-fry](https://aur.archlinux.org/packages/fatscript-fry) AUR package.

For other distributions, try the auto-install script:

```bash
curl -sSL https://gitlab.com/fatscript/fry/raw/main/get_fry.sh -o get_fry.sh;
bash get_fry.sh || sudo bash get_fry.sh
```

Or, to install `fry` manually:

- Clone the repository:

```bash
git clone --recursive https://gitlab.com/fatscript/fry.git
```

- Then, run the installation script:

```bash
cd fry
./install.sh
```

> the manual installation may copy the `fry` binary to the $HOME/.local/bin folder, alternatively use `sudo` to install it to /usr/local/bin/

- Verify that `fry` is installed by running:

```bash
fry --version
```

### Dependencies

If the installation fails, you may be missing some dependencies. `fry` requires `git`, `gcc` and `libcurl` to build. For example, to install these dependencies on Debian/Ubuntu, run:

```bash
apt update
apt install git gcc libcurl4-openssl-dev
```

#### Back-end for text input

`linenoise` is a lightweight dependency and an alternative to `readline`, maintained as a submodule. If it was not included during the initial `git clone` operation, you can rectify this with the following commands:

```bash
git submodule init
git submodule update
```

If you prefer to link against `readline`, just ensure it is installed by running:

```bash
apt install libreadline-dev
```

## OS Support

`fry` is primarily designed for GNU/Linux, but it's also accessible on other operating systems:

### Android

If you're on Android, you can install `fry` via [Termux](https://termux.dev/). Just install the required dependencies like so:

```bash
pkg install git clang
```

Then you can follow the standard installation instructions for `fry`.

### ChromeOS

If you're using ChromeOS, you can enable Linux support by following the instructions [here](https://chromeos.dev/en/linux/setup).

### MacOS

If you're using MacOS, you'll need to have [Command Line Tools](https://developer.apple.com/forums/thread/670389) installed.

### iOS

If you're using iOS, you may use `fry` via [iSH](https://github.com/ish-app/ish/). First, install the required dependencies:

```bash
apk add bash gcc libc-dev curl-dev
```

Then, according to [this thread](https://github.com/ish-app/ish/issues/943), configure `git` to work properly, like so:

```bash
wget https://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86/git-2.24.4-r0.apk
apk add ./git-2.24.4-r0.apk
git config --global pack.threads "1"
```

### Windows

If you're using Windows, you can use `fry` via [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en/windows/wsl/install).

## Docker image

`fry` is also available as a [docker image](https://hub.docker.com/r/fatscript/fry/tags):

```bash
docker run --rm -it fatscript/fry
```

To execute a FatScript file with docker, use the following command:

```bash
docker run --rm -it -v ~/project:/app fatscript/fry prog.fat
```

> replace ~/project with the path to your FatScript file

## Troubleshooting

If you encounter any issues or bugs while using `fry`, please [open an issue](https://gitlab.com/fatscript/fry/issues).
