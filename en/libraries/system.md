{% if output.name != "ebook" %}

# system

{% endif %}

System-level operations and information

## Import

```
_ <- fat.system
```

## Alias

- ArgValues: a list of arguments (Text) from command line

## Types

| Name          | Signature                   | Brief                  |
| ------------- | --------------------------- | ---------------------- |
| CommandResult | (code: ExitCode, out: Text) | Return type of capture |

## Constants

- successCode, 0: ExitCode
- failureCode, 1: ExitCode

## Methods

| Name      | Signature                        | Brief                                  |
| --------- | -------------------------------- | -------------------------------------- |
| args      | <> ArgValues                     | Return list of args passed from shell  |
| exit      | (code: Number): \*               | Exit program with provided exit code   |
| getEnv    | (var: Text): Text                | Get env variable value by name         |
| shell     | (cmd: Text): ExitCode            | Execute cmd in shell, return exit code |
| capture   | (cmd: Text): CommandResult       | Capture the output of cmd execution    |
| fork      | (args: List/Text, out: Text = ø) | Start background process, return PID   |
| kill      | (pid: Number): Void              | Send SIGTERM to process by PID         |
| getLocale | <> Text                          | Get current locale setting             |
| setLocale | (\_: Text): Number               | Set current locale setting             |
| getMacId  | <> Text                          | Get machine identifier (MAC address)   |
| blockSig  | (enabled: Boolean): Void         | Block SIGINT, SIGHUP and SIGTERM       |

## Usage notes

### Heads Up!

It is important to exercise caution and responsibility when using the `getEnv`, `shell`, `capture`, `fork` and `kill` methods. The `system` library provides the capability to execute commands directly from the operating system, which can introduce security risks if not used carefully.

To mitigate potential vulnerabilities, avoid using user input directly in constructing commands passed to these methods. User input should be validated to prevent command injection attacks and other security breaches.

### Handling signals

When a Ctrl+C interruption occurs, the FatScript interpreter's main thread captures the signal and initiates a cleanup process. During this process, if it detects any running [Workers](async.md), it will forcibly terminate them to prevent the application from hanging.

For applications requiring more refined control over the shutdown process, FatScript provides an option to block the default signal handling by setting `system.blockSig(true)`. When enabled, the interpreter will not capture Ctrl+C. This requires you to implement your own termination mechanisms, possibly via `curses.readKey` or another method.

### Other Limitations (multithreading)

While the methods in this library support a variety of programming tasks, they are not optimized for interleaved usage within asynchronous `Workers`. When initiating processes from within threads, opt for `shell/capture` methods, or exclusively use `fork/kill`. Mixing these two method pairs in multithreaded applications can result in unpredictable behavior.

> on each call, `shell/capture` will set `SIGCHLD` to its default behavior, while `fork` will ignore this signal to try to avoid zombie processes

### fork

The `out` parameter allows redirecting the standard output (`stdout`) to a specified output file. If you wish to discard this output, you can use "/dev/null" as an argument.

### get/set locale

The `fry` interpreter will attempt to initialize `LC_ALL` locale to `C.UTF-8` and if that locale is not available on the system tries to use `en_US.UTF-8`, otherwise, the default locale will be used.

See more about [locale names](https://www.gnu.org/software/libc/manual/html_node/Locale-Names.html).

> locale configuration applies only to application, and is not persisted after `fry` exits
