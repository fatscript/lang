{% if output.name != "ebook" %}

# Embedded commands

{% endif %}

Embedded commands are FatScript's low-level functions that can be invoked with keywords preceded by a dollar sign `$`. These commands are always available, implemented as compiled code, and require no imports.

Unlike methods, they take no explicit arguments, but may read from specific entry names in the current scope, or even from the interpreter's internal state.

## Handy ones

Here a are some embedded commands that could be useful to know:

- `$break` pauses execution and loads the debugging console
- `$debug` toggles interpreter debug logs (only in some builds)
- `$exit` exits program with provided code
- `$keepDotFry` keeps the config (.fryrc) in scope after startup
- `$result` toggles result printing at the end of execution
- `$root` provides a reference to global scope
- `$self` provides a self reference to method/instance scope
- `$bytesUsage` returns maximum of bytes allocated
- `$nodesUsage` returns total of nodes allocated at the moment
- `$isMain` checks if code is executing as main or module
- `$port` retrieves actual port number assigned to the HTTP server

> `root` and `self` keywords are automatically lifted into `$root` and `$self`

You can call those directly on your code, like:

```
$exit  # terminates the program
```

> in order to use other embedded commands you have to study the C implementation of `fry`, as the complete list is not documented, refer to [embedded.c](https://gitlab.com/fatscript/fry/blob/main/src/sdk/embedded.c) file

## Libs under the hood

Standard libraries wrap embedded calls into methods, providing a more ergonomic interface. You don't need to create an execution scope or load arguments into that scope before delegating execution to them.

For example, here's how you can use the `floor` method from [math lib](math.md):

```
_ <- fat.math
floor(2.53)
```

This method is implemented as:

```
floor = (x: Number): Number -> $floor
```

Under the hood, the `floor` method creates an execution scope and loads an argument as `x` into it. The method then delegates execution to the `$floor` embedded command, which reads the value of `x` from the current scope and returns the floor of that number.

You can achieve the same outcome as above method by doing the following:

```
x = 2.53
$floor  # reads value of x from current scope
```

## Hacking

You can see which embedded command a library method is calling by looking into the library's implementation via the `readLib` method from the [SDK lib](sdk.md). Technically, there is nothing preventing you from calling embedded commands directly.

For example, you could terminate your program by calling `$exit` directly, which will exit with code 0 (default) or, if a numeric entry named `code` exists in the current scope, the value of that entry will be used as the exit code. However, it would be more elegant to import the `fat.system` library and call the `exit` method with the desired exit code:

```
sys <- fat.system
sys.exit(0)  # exits with code 0
```

This approach makes your code more readable and less prone to errors, and it also provides a better separation of concerns.

It's important to keep in mind that embedded commands are black boxes and not intended for writing common FatScript code. In most cases, you would need to read the [underlying C implementation](https://gitlab.com/fatscript/fry/tree/main/src/runtime/embed) to better grasp what a command is actually doing.

While it's possible to use embedded commands to gain additional runtime performance by avoiding imports and method calls, this is not recommended due to the loss of code readability. In general, it's better to use the standard libraries and follow best practices for writing clear, maintainable code.
