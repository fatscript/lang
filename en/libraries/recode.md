{% if output.name != "ebook" %}

# recode

{% endif %}

Data conversion between various formats

## Import

```
_ <- fat.recode
```

> [type package](type/index.md) is automatically imported with this import

## Variables

These settings can be used to adjust the behavior of the processing functions:

- csvSeparator, default is `,` (comma)
- csvQuote, default is `"` (double quote)

## Base64 functions

| Name       | Signature           | Brief                                 |
| ---------- | ------------------- | ------------------------------------- |
| toBase64   | (data: Chunk): Text | Encode binary chunk to base64 text    |
| fromBase64 | (b64: Text): Chunk  | Decode base64 text to original format |

## JSON functions

| Name     | Signature         | Brief                         |
| -------- | ----------------- | ----------------------------- |
| toJSON   | (val: Any): Text  | Encode JSON from native types |
| fromJSON | (json: Text): Any | Decode JSON to native types   |

> with `toJSON` the native types such as `HugeInt`, `Method`, and `Chunk` will translate into `null`, while `Errors` will be converted to text

## URL functions

| Name         | Signature           | Brief                                      |
| ------------ | ------------------- | ------------------------------------------ |
| toURL        | (text: Text): Text  | Encode text to URL escaped text            |
| fromURL      | (url: Text): Text   | Decode URL escaped text to original format |
| toFormData   | (data: Scope): Text | Encode scope to URL encoded Form Data      |
| fromFormData | (data: Text): Scope | Decode URL encoded Form Data to scope      |

## CSV functions

| Name    | Signature                                   | Brief                |
| ------- | ------------------------------------------- | -------------------- |
| toCSV   | (header: List/Text, rows: List/Scope): Text | Encode CSV from rows |
| fromCSV | (csv: Text): List/Scope                     | Decode CSV into rows |

> starting with version `4.x.x` CSV methods support automatic quoting, escaped quotes, separators and new lines within quotes

## RLE functions

| Name    | Signature             | Brief                      |
| ------- | --------------------- | -------------------------- |
| toRLE   | (chunk: Chunk): Chunk | Compress to RLE schema     |
| fromRLE | (chunk: Chunk): Chunk | Decompress from RLE schema |

## Frost and hot copies

| Name        | Signature        | Brief                          |
| ----------- | ---------------- | ------------------------------ |
| toFrostCopy | (item: Any): Any | Creates immutable copy of item |
| toHotCopy   | (item: Any): Any | Creates mutable copy of item   |

> `toFrostCopy` ensures immutability, ideal for capturing "safe" nested data snapshots, while `toHotCopy` allows data to be heated up again, useful where frozen data needs further processing

## Other functions

| Name      | Signature         | Brief                               |
| --------- | ----------------- | ----------------------------------- |
| inferType | (val: Text): Any  | Convert text to void/boolean/number |
| minify    | (src: Text): Text | Minifies FatScript source code      |

> `minify` will replace any `$break` statements (debugger breakpoint) with `()`

To disable the type inference provided by `inferType` for `fromFormData` and `fromCSV`, you can override it globally by using `recode.inferType = -> _` after importing `fat.recode`, or to reactivate it use `recode.inferType = val -> $inferType`.

## See also

- [Type package](type/index.md)
- [SDK library](sdk.md)
