{% if output.name != "ebook" %}

# math

{% endif %}

Mathematical operations and functions

## Import

```
_ <- fat.math
```

## Constants

- e, natural logarithm constant 2.71...
- maxInt, 9007199254740992
- minInt, -9007199254740992
- pi, ratio of circle to its diameter 3.14...

> read more about [number precision](../syntax/types/number.md#precision) in FatScript

## Basic functions

| Name   | Signature                             | Brief                                  |
| ------ | ------------------------------------- | -------------------------------------- |
| abs    | (x: Number): Number                   | Return absolute value of x             |
| ceil   | (x: Number): Number                   | Return smallest integer >= x           |
| floor  | (x: Number): Number                   | Return largest integer <= x            |
| isInf  | (x: Number): Boolean                  | Return true if x is infinity           |
| isNaN  | (x: Any): Boolean                     | Return true if x is not a number       |
| logN   | (x: Number, base: Number = e): Number | Return logarithm of x                  |
| random | <> Number                             | Return pseudo-random, where 0 <= n < 1 |
| sqrt   | (x: Number): Number                   | Return the square root of x            |
| round  | (x: Number): Number                   | Return the nearest integer to x        |

## Trigonometric functions

| Name     | Signature                  | Brief                          |
| -------- | -------------------------- | ------------------------------ |
| sin      | (x: Number): Number        | Return the sine of x           |
| cos      | (x: Number): Number        | Return the cosine of x         |
| tan      | (x: Number): Number        | Return the tangent of x        |
| asin     | (x: Number): Number        | Return the arc sine of x       |
| acos     | (x: Number): Number        | Return the arc cosine of x     |
| atan     | (x: Number, y = 1): Number | Return the arc tangent of x, y |
| radToDeg | (r: Number): Number        | Convert radians to degrees     |
| degToRad | (d: Number): Number        | Convert degrees to radians     |

## Hyperbolic functions

| Name | Signature           | Brief                              |
| ---- | ------------------- | ---------------------------------- |
| sinh | (x: Number): Number | Return the hyperbolic sine of x    |
| cosh | (x: Number): Number | Return the hyperbolic cosine of x  |
| tanh | (x: Number): Number | Return the hyperbolic tangent of x |

## Statistical functions

| Name     | Signature                | Brief                                     |
| -------- | ------------------------ | ----------------------------------------- |
| mean     | (v: List/Number): Number | Return the mean of a vector               |
| median   | (v: List/Number): Number | Return the median of a vector             |
| sigma    | (v: List/Number): Number | Return the standard deviation of a vector |
| variance | (v: List/Number): Number | Return the variance of a vector           |
| max      | (v: List/Number): Number | Return maximum value in vector            |
| min      | (v: List/Number): Number | Return the minimum value in vector        |
| sum      | (v: List/Number): Number | Return the sum of vector                  |

## Other functions

| Name    | Signature           | Brief                             |
| ------- | ------------------- | --------------------------------- |
| fact    | (x: Number): Number | Return the factorial of x         |
| exp     | (x: Number): Number | Return e raised to the power of x |
| sigmoid | (x: Number): Number | Return the sigmoid of x           |
| relu    | (x: Number): Number | Return the ReLU of x              |

### Example

```
math <- fat.math  # named import
math.abs(-52)     # yields 52
```

## See also

- [Number (syntax)](../syntax/types/number.md)
- [Number prototype extensions](type/number.md)
