{% if output.name != "ebook" %}

# color

{% endif %}

ANSI color codes for console

## Import

```
_ <- fat.color
```

## Constants

- black, 0
- red, 1
- green, 2
- yellow, 3
- blue, 4
- magenta, 5
- cyan, 6
- white, 7
- bright.black, 8
- bright.red, 9
- bright.green, 10
- bright.yellow, 11
- bright.blue, 12
- bright.magenta, 13
- bright.cyan, 14
- bright.white, 15

## Methods

| Name        | Signature                               | Brief                         |
| ----------- | --------------------------------------- | ----------------------------- |
| detectDepth | <> Number                               | Get console color support     |
| to8         | (xr: Any, g: Number = ø, b: Number = ø) | Convert RGB to 8-color mode   |
| to16        | (xr: Any, g: Number = ø, b: Number = ø) | Convert RGB to 16-color mode  |
| to256       | (xr: Any, g: Number = ø, b: Number = ø) | Convert RGB to 256-color mode |
| to24Bit     | (xr: Any, g: Number = ø, b: Number = ø) | Encode RGB for true color     |

## Usage notes

### to8, to16, to256 and to24Bit

The parameter `xr` can be an optional text representing the color in HTML format. For example, it can be provided as 'fae830' or '#fae830' (yellow):

```
color   <- fat.console
console <- fat.console

console.log('hey', color.to16('fae830'))
console.log('hey', color.to256('fae830'))
```

However, if `xr` is a number between 0 and 255 representing `r`, then the `g` and `b` parameters will be required:

```
console.log('hey', color.to256(250, 232, 48))  // same result
```

> these methods may produce approximations of the original color in 8, 16 or 256 depths and not the exact true color

## See also

- [Console library](console.md)
- [Curses library](curses.md)
- [256 Colors](https://www.ditig.com/256-colors-cheat-sheet)
