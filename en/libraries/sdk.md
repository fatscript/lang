{% if output.name != "ebook" %}

# sdk

{% endif %}

Fry's software development kit utilities

> a special library that exposes some of the inner elements of fry interpreter

## Import

```
_ <- fat.sdk
```

## Methods

| Name       | Signature             | Brief                                  |
| ---------- | --------------------- | -------------------------------------- |
| ast        | (\_): Void            | Print abstract syntax tree of node     |
| stringify  | (\_): Text            | Serialize node into JSON-like text     |
| eval       | (\_): Any             | Interpret text as FatScript program    |
| getVersion | <> Text               | Return fry version                     |
| printStack | (depth: Number): Void | Print execution context stack trace    |
| readLib    | (ref: Text): Text     | Return fry library source code         |
| typeOf     | (\_): Text            | Return type name of node               |
| getTypes   | <> List               | Return info about declared types       |
| getDef     | (name: Text): Any     | Return type definition by name         |
| getMeta    | <> Scope              | Return interpreter's metadata          |
| setKey     | (key: Text): Void     | Set key for obfuscated bundles         |
| setMem     | (n: Number): Void     | Set memory limit (node count)          |
| runGC      | <> Number             | Run GC, return elapsed in milliseconds |
| quickGC    | <> Number             | Run single GC cycle and return ms      |

## Usage notes

### stringify

While `recode.toJSON` outputs strictly valid JSON, `stringify` is more lax. It is capable of exporting `HugeInt` as hexadecimal numbers (e.g., `0x123abc`), `Chunk` as Base64 encoded, and other types may also have representations more informative than just `null`. These representations are designed to allow a richer export for the FatScript environment and are not intended for JSON-compliant serialization.

## readLib

```
_ <- fat.sdk
_ <- fat.console

print(readLib('fat.extra.Date'))  # prints the Date library implementation
```

> `readLib` cannot see external files, but `read` from [file lib](file.md) can

### setKey

Use preferably on `.fryrc` file like so:

```
_ <- fat.sdk
setKey('secret')  # will encode and decode bundles with this key
```

See more about [obfuscating](../general/bundling.md#obfuscating).

### setMem

Use preferably on `.fryrc` file like so:

```
_ <- fat.sdk
setMem(5000)  # ~2mb
```

### Choosing between full and quick GC

Most simple scripts in FatScript won't need to worry about memory management, as the default settings are designed to provide ample memory capacity and efficient automatic behavior from the start. Generally, the best way to optimize performance is by simply adjusting the memory limit. In some rare cases, such as a game loop or complex iterative processes, you may benefit from explicitly calling the GC.

The `quickGC` method performs a quick and less exhaustive cleanup, making it suitable for scenarios where some flexibility in memory allocation is acceptable. On the other hand, `runGC` ensures a more complete garbage collection, but it can result in longer runtimes depending on factors such as the size and complexity of the memory graph. However, `quickGC` may lead to the accumulation of unclaimed memory, making it less effective in certain contexts. The best way to determine the most appropriate option is to perform comparative tests on your application, simulating real-use scenarios.

> run your script with the `-c` flag to benchmark its execution

See more about [memory management](../general/options.md#memory-management).

## See also

- [Recode library](recode.md)
