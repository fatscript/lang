{% if output.name != "ebook" %}

# socket

{% endif %}

TCP socket manipulation

> optionally supports SSL/TLS (if the OpenSSL library is available)

## Import

```
_ <- fat.socket
```

## Structures

### ClientSocket

Represents a client connection, i.e., a socket connected to a remote server.

#### Properties

| Name | Type   | Description                            |
| ---- | ------ | -------------------------------------- |
| id   | Number | Internal file descriptor of the socket |
| peer | Text   | Metadata about the remote endpoint     |
| ssl  | Chunk  | Internally used for SSL sessions       |

#### Methods

| Name    | Signature               | Brief description              |
| ------- | ----------------------- | ------------------------------ |
| receive | (nBytes: Number): Chunk | Reads up to nBytes from socket |
| send    | (data: Chunk): Void     | Sends data to the socket       |
| close   | <> Void                 | Closes the connection          |

> `receive(nBytes)` will block until it obtains some data or until the connection is closed

### ServerSocket

Represents a socket in server mode, which waits for client connections.

#### Properties

| Name | Type   | Description                            |
| ---- | ------ | -------------------------------------- |
| id   | Number | Internal file descriptor of the socket |

#### Methods

| Name   | Signature                    | Brief description                        |
| ------ | ---------------------------- | ---------------------------------------- |
| accept | (wait: Number): ClientSocket | Waits for a connection for up to wait ms |
| close  | <> Void                      | Closes the server socket                 |

## Standalone methods

| Name      | Signature                                  | Brief description                 |
| --------- | ------------------------------------------ | --------------------------------- |
| connect   | (addr, port, useSSL = false): ClientSocket | Connects to a remote server       |
| verifySSL | (enabled: Boolean): Void                   | SSL configuration (client mode)   |
| setSSL    | (certPath: Text, keyPath: Text): Void      | SSL configuration (server mode)   |
| bind      | (port: Number): ServerSocket               | Starts a server on the given port |

## Usage notes

### Client mode

#### Establishing a connection

Use the `connect(addr, port, useSSL)` function to create a `ClientSocket`. For example:

```
_      <- fat.type._
socket <- fat.socket

con = socket.connect("example.org", 80)
con.send("GET / HTTP/1.1\r\nHost: example.org\r\n\r\n".toChunk)
response = con.receive(1024)
...
con.close
```

#### Certificate verification

- To enable SSL/TLS, pass `true` to the `useSSL` parameter in `connect`.
- By default, certificate verification is active when using SSL. If you use self-signed certificates on your test server, you can call `verifySSL(false)` to disable verification:

```
socket.verifySSL(false)
con = socket.connect("localhost", 443, true)
...
```

#### Reading and writing

You can use `send` to send data and `receive` to read data. `send` operations delegate sending to the operating system and return immediately, while `receive` operations block until some message is received or the connection is closed by the other party.

### Server mode

#### Listening on a port

To start a socket in server mode, use `bind(port)`, obtaining a `ServerSocket` object:

```
socket <- fat.socket

server = socket.bind(1234)
...
```

#### Accepting connections

- Call `server.accept(wait)` to accept client connections. If no connection appears within `wait` milliseconds, it returns `null`.
- Upon obtaining a `ClientSocket` from `accept`, you can interact with the client using `receive`, `send`, and `close`.

```
~ con = server.accept(5000)  # waits up to 5 seconds
con != null ? {
  msg = con.receive(256)
  ...
  con.send(Chunk("Hello, client!"))
  con.close
}
```

> use wait of `-1` for blocking until a new connection starts, use wait of `0` for non-blocking

#### SSL on the server

Before calling `bind`, use `setSSL("cert.pem", "key.pem")` to load a certificate and private key and enable SSL/TLS:

```
socket.setSSL("cert.pem", "key.pem")
server = socket.bind(443)
```

#### Closing the server

Simply call `server.close` when you want to stop accepting new connections.

## See also

- [Async library](async.md)
- [HTTP library](http.md)
