{% if output.name != "ebook" %}

# enigma

{% endif %}

Cryptography, hash and UUID methods

## Import

```
_ <- fat.enigma
```

## Standard methods

These methods are available on all `fry` builds. Although `derive`, `encrypt`, and `decrypt` help create "non-human-readable" ciphertext, they are not considered cryptographically secure. **DO NOT use them alone to protect sensitive data!**

| Name    | Signature                                      | Brief                         |
| ------- | ---------------------------------------------- | ----------------------------- |
| getHash | (msg: Text): Number                            | Compute a 32-bit hash of text |
| genUUID | <> Text                                        | Generate a UUID (version 4)   |
| genKey  | (len: Number): Text                            | Generate a random key         |
| derive  | (secret: Text, salt: Text, iter: Number): Text | Basic key derivation function |
| encrypt | (msg: Text, key: Text = ø): Text               | Encrypt message using key     |
| decrypt | (msg: Text, key: Text = ø): Text               | Decrypt message using key     |

## OpenSSL methods

These methods are cryptographically safe and only available in builds that include `OpenSSL` support. They provide robust tools for handling encryption, decryption, hashing, and key derivation with high-security standards.

| Name       | Signature                                                     | Brief                          |
| ---------- | ------------------------------------------------------------- | ------------------------------ |
| digest     | (data: Chunk, algo: Text = 'sha256'): HugeInt                 | Compute a secure hash          |
| bytes      | (len: Number): Chunk                                          | Generate random bytes          |
| pbkdf2     | (secret: Text, salt: Text, iter: Number, algo: Text): HugeInt | Derive key using PBKDF2        |
| hmac       | (data: Chunk, key: Chunk, algo: Text): HugeInt                | Compute auth-code              |
| encryptAES | (data: Chunk, key: Chunk): Chunk                              | Encrypt data with AES-256 key  |
| decryptAES | (data: Chunk, key: Chunk): Chunk                              | Decrypt AES-256 encrypted data |

## Usage notes

### getHash

A 32-bit hash is sufficient to protect against data corruption in up to 100kb. Fry uses `FNV1A_Jesteress`, which is one of the fastest and "good enough" hash algorithm for "long" strings.

### genUUID

A UUID, or Universally Unique Identifier, is a 128-bit number used to identify objects or entities in computer systems. The implementation generates random UUIDs following the format of version 4 RFC 4122 specification, though it does not strictly adhere to cryptographically secure randomness standards.

### genKey

Generates a random key using the Base64 alphabet.

### derive (unsafe)

This deterministic key derivation function outputs a 32-character string using the Base64 alphabet. While it may appear to offer 192 bits of entropy, the effective entropy **is significantly lower** due to the underlying hashing function.

> Note: The original intention of this function is also key stretching. It is designed to be used in conjunction with the `encrypt` and `decrypt` functions, providing an additional layer of processing for the keys used in these cryptographic operations. However, as the method does not provide robust cryptographic security by itself, it is recommended to use more secure key derivation methods for applications that require high standards of data protection.

### encrypt/decrypt (unsafe)

These functions can be used with or without a specified key (blank for default key). They employ a simple XOR operation combined with a control hash and are encoded in Base64. These methods are **not cryptographically secure** and should not be used for protecting sensitive data, unless combined with a [one-time pad](https://en.wikipedia.org/wiki/One-time_pad).

## Additional details for OpenSSL methods

### digest

Supports multiple hash algorithms including sha1, sha224, sha256, sha384, sha512, sha3_224, sha3_256, sha3_384, sha3_512, allowing flexibility depending on security requirements.

### bytes

Ideal for creating high-entropy cryptographic keys and initialization vectors (IVs).

### pbkdf2

Utilizes a password, salt, and specified number of iterations along with a cryptographic hash function to produce a robust encryption key.

### hmac

Ensures data integrity and authenticity using a secret key and the specified hash function.

### encryptAES/decryptAES

Operate using AES-256-CCM mode, providing confidentiality and authenticity. The key should be precisely 32 bytes long, obtained from a secure source like the pbkdf2 function, this can typically be achieved like:

```
key: HugeInt = enigma.pbkdf2(...)
encryptionKey = key.toChunk.fit(32)
```
