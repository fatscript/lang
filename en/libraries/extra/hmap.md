{% if output.name != "ebook" %}

# HashMap

{% endif %}

An optimized in-memory key-value store, serving as a better performance replacement for default Scope implementation, designed for handling large data sets efficiently.

> the speed gains will come at the expense of more memory usage

## Import

```
_ <- fat.extra.HashMap
```

## Constructor

| Name    | Signature               | Brief                                      |
| ------- | ----------------------- | ------------------------------------------ |
| HashMap | (capacity: Number = 97) | Create a HashMap with a specified capacity |

> the default capacity of 97 is generally efficient for up to 10,000 items

### Capacity Optimization

Ideally, you should keep at most about 100 items per 'bucket' in the hash table. In this context, 'capacity' refers to the number of buckets available for your data. Note that this implementation does not automatically adjust its size, so proper initial sizing is crucial. The following table can help determine the optimal capacity for storing `n` items:

```
n <   5000 => 53
n <  10000 => 97
n <  20000 => 193
n <  40000 => 389
n <  80000 => 769
n < 160000 => 1543
_          => 3079
```

> using prime numbers can help reduce collisions

These values are based on empirical tests and should be adjusted according to your specific data needs and performance goals. Keep in mind that the relationship between capacity and performance is not entirely linear; as the number of items increases, the benefits of further increasing the capacity diminish.

### Recommendation

Although the standard FatScript Scope exhibits slower performance for insertions, it excel in data retrieval and updates, outperforming `HashMap` for small collections (under ~500 items). Therefore, the benefits of using `HashMap` are most noticeable in scenarios involving frequent inserts on large data sets.

## Prototype members

| Name     | Signature                    | Brief                                     |
| -------- | ---------------------------- | ----------------------------------------- |
| isEmpty  | <> Boolean                   | Return true if length is zero             |
| nonEmpty | <> Boolean                   | Return true if length is non-zero         |
| size     | <> Number                    | Return hash table length                  |
| toText   | <> Text                      | Return 'HashMap/capacity' as text literal |
| freeze   | <> Void                      | Make the value immutable                  |
| set      | (key: Text, value: Any): Any | Set a key-value pair in the HashMap       |
| get      | (key: Text): Any             | Get the value associated with a key       |
| keys     | <> Keyset                    | Return a list of all keys in the HashMap  |

### Example

```
_ <- fat.extra.HashMap

hmap = HashMap()
hmap.set('key1', 'value1')

hmap.get('key1')  # yields 'value1'
hmap.keys         # yields [ 'key1' ]
```
