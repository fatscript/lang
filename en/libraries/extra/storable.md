{% if output.name != "ebook" %}

# Storable

{% endif %}

Data store facilities

## Import

```
_ <- fat.extra.Storable
```

> [file library](../file.md), [sdk library](../sdk.md), [enigma library](../enigma.md), [Error type](../type/error.md), [Text type](../type/text.md), [Void type](../type/void.md) and [Method type](../type/method.md) are automatically imported with this import

## Mixins

This library introduces two mixin types: `Storable` and `EncryptedStorable`

### Storable

The `Storable` mixin provides methods for storing and retrieving objects in the filesystem using JSON serialization.

#### Prototype members

| Name  | Signature       | Brief                                  |
| ----- | --------------- | -------------------------------------- |
| list  | <> Keyset       | Get list of ids for stored instances   |
| load  | (id: Text): Any | Load an object from the filesystem     |
| save  | <> Boolean      | Save the current object instance       |
| erase | <> Boolean      | Delete the file associated with the id |

> the `load` and `save` methods throw `FileError` on failure

### EncryptedStorable

Extends `Storable` with encryption capabilities for safer data storage. Requires an implementation of `getEncryptionKey` method.

## Usage example

```
_ <- fat.extra.Storable

# Define a type that includes Storable (or EncryptedStorable)
User = (
  Storable  # Include the Storable mixin

  # EncryptedStorable                             # alternative implementation
  # getEncryptionKey = (): Text -> '3ncryp1ptM3'  # could get via KMS or config

  ## Argument slots
  name: Text
  email: Text

  # Setters return new immutable instance copy with updated field
  setName = (name: Text) -> self + User * { name }
  setEmail = (email: Text) -> self + User * { email }
)

# Create a new user instance
newUser = User('Jane Doe', 'jane.doe@example.com')

# Save the new user
newUser.save

# Update a user's information and save the changes
updatedUser = newUser
  .setName('Jane Smith')
  .setEmail('jane.smith@example.com')
updatedUser.save

# List all saved users
userIds = User.list

# Load a user from the filesystem
userId = userIds(0)  # ...or newUser.id
loadedUser = User.load(userId)

# Delete user's data from the filesystem
loadedUser.erase  # ...or User.erase(userId)
```

### Storable in Web Build

When using `fry` built with Emscripten (for example, when using [FatScript Playground](https://fatscript.org/playground)), this prototype uses embedded commands `$storableSet`, `$storableGet`, `$storableList`, and `$storableRemove`, which are only defined in the web build. Therefore, instead of using the conventional file system for storage, there is special support for using the browser's `localStorage` object.

## See also

- [Extra package](index.md)
