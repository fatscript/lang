{% if output.name != "ebook" %}

# MouseEvent

{% endif %}

Mouse event parser for `fat.curses.readKey`

## Import

```
_ <- fat.extra.MouseEvent
```

## Constructor

| Name       | Signature   | Brief                  |
| ---------- | ----------- | ---------------------- |
| MouseEvent | (val: Text) | Parses a readKey event |

The `MouseEvent` constructor takes the following argument:

- **val**: The text value returned from `fat.curses.readKey`, which is parsed to extract mouse event data (like click type, position, and release state).

> if `val` is not a valid mouse event, `MouseEvent` returns `null`

## Prototype members

| Name      | Signature | Brief                                    |
| --------- | --------- | ---------------------------------------- |
| name      | <> Text   | Get human-readable name for mouse action |
| code      | Text      | The code representing the mouse action   |
| x         | Number    | The X-coordinate of the mouse            |
| y         | Number    | The Y-coordinate of the mouse            |
| isRelease | Boolean   | Is the event a button release?           |

### Event naming

The `name` method can return the following human-readable values based on the event code and modifier keys:

- **Press Actions**: `leftPress`, `middlePress`, `rightPress`
- **Release Actions**: `leftRelease`, `middleRelease`, `rightRelease`
- **Drag Actions**: `leftDrag`, `middleDrag`, `rightDrag`
- **Scroll Actions**: `scrollUp`, `scrollDown`
- **Modifiers**: `Shift+`, `Alt+`, `Ctrl+` (prefixed to the above actions)

## Example

`MouseEvent` is useful for converting raw event strings into usable data, like mouse position and action:

```
_       <- fat.extra.MouseEvent
console <- fat.console
curses  <- fat.curses

# Enable mouse tracking
curses.setMouse(true)

# Capture a curses events
(~ key = curses.readKey) == Void @ {
  # Wait for an event to happen... (no-op)
}

# Parse the curses event
(mEvt = MouseEvent(key)) => {
  console.log('Mouse event:')
  console.log('  Code: {mEvt.code}')
  console.log('  X: {mEvt.x}')
  console.log('  Y: {mEvt.y}')
  console.log('  Released: {mEvt.isRelease}')
  console.log('  Action: {mEvt.name}')
}
_ => console.log('Non-mouse event: {key}')
```

## See also

- [Curses library](../curses.md)
