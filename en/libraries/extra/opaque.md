{% if output.name != "ebook" %}

# Opaque

{% endif %}

Utility for soft protection of data

## Import

```
_ <- fat.extra.Opaque
```

> [Scope type](../type/scope.md) is automatically imported along with this import

## Prototype

This library introduces the `Opaque` type, designed as a utility for encapsulating data with soft protection. It provides a wrapper around a `Scope`, preventing direct access to the underlying structure when serializing while allowing controlled with its members.

The `Opaque` type extends the `Scope` type, inheriting all its members without introducing additional features.

## Usage example

```
visible = { ~ a = 1, b = 2 }

# Creating an opaque wrapper
opaque = Opaque(visible)

# Modifying values in the hidden scope
opaque.c = 3  # the same as regular scope

# The hidden scope is excluded from serialization at the top level
recode.toJSON({ opaque, visible })  # '{"opaque":null,"visible":{"a":1,"b":2}}'

# Direct serialization of the hidden scope would still work
recode.toJSON(opaque)  # '{"a":1,"b":2,"c":3}'
```

### Performance notes

The `Opaque` type introduces a layer of indirection that could impact performance in scenarios involving frequent access or modifications to the hidden data. Consider using it selectively for scenarios requiring encapsulation, e.g. storing credentials within an instance.

### See also

- [Scope type](../type/scope.md)
- [Recode library](../recode.md)
