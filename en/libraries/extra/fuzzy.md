{% if output.name != "ebook" %}

# Fuzzy

{% endif %}

Probabilistic values and fuzzy logic operations

## Import

```
_ <- fat.extra.Fuzzy
```

## Constructor

| Name  | Signature           | Brief                            |
| ----- | ------------------- | -------------------------------- |
| Fuzzy | (val: Number = 0.5) | Create a Fuzzy probability value |

> the range from 0 to 1 is ideal for values, however, higher values can still be meaningful in specific operations like conjunction with values within the standard range

## Prototype members

| Name     | Signature             | Brief                                          |
| -------- | --------------------- | ---------------------------------------------- |
| isEmpty  | <> Boolean            | Check if probability is zero                   |
| nonEmpty | <> Boolean            | Check if probability is greater than zero      |
| size     | <> Number             | Convert fuzzy value to a percentage scale      |
| toText   | <> Text               | Convert fuzzy value to a textual percentage    |
| freeze   | <> Void               | Make the value immutable                       |
| and      | (other: Fuzzy): Fuzzy | Logical AND operation with another fuzzy value |
| or       | (other: Fuzzy): Fuzzy | Logical OR operation with another fuzzy value  |
| not      | <> Fuzzy              | Logical NOT operation, inverting the chance    |
| decide   | <> Boolean            | Decide a boolean outcome within its chance     |

## Usage

```
_ <- fat.extra.Fuzzy

# Creating fuzzy instances
lowChance = Fuzzy(0.25)  # 25% chance
highChance = Fuzzy(0.75) # 75% chance

# Applying logical operations
combinedChance = lowChance.and(highChance)
resolvedChance = combinedChance.decide  # results in a boolean
```

### Inspiration

Introducing the `Fuzzy` type into `FatScript` was inspired by the humorous meme language definition, [DreamBerd](https://github.com/TodePond/DreamBerd), which offers booleans that can be `true`, `false`, or `maybe`. Here, the `maybe` keyword translates to `Fuzzy().decide`, which can be considered an uncommon construct for most programming languages and is analogous to flipping a coin.

Although `FatScript` is not as esoteric to the extent of storing booleans as "one-and-a-half bits", the concept of providing a "funny" type that allows for modeling uncertainty was an interesting experiment and might actually prove useful in many scenarios. It enhances the language's capabilities to handle operations involving chances and decision-making processes where outcomes are not deterministic. The `Fuzzy` type is useful for scenarios requiring a nuanced approach to boolean logic, commonly seen in gaming logic, and anywhere probabilistic decisions are needed.

## See also

- [Boolean type](../type/boolean.md)
- [Math library](../library/math.md)
