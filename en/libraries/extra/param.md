{% if output.name != "ebook" %}

# Param

{% endif %}

Parameter presence and type verification

## Import

```
_ <- fat.extra.Param
```

> [Error type](../type/error.md) is automatically imported with this import

## Types

This library introduces the `Param` type and the `Using/UsingStrict` utility for implicit parameter declaration.

## Constructors

Both `Param` and `Using/UsingStrict` constructors take two arguments:

- **\_exp: Text**: the parameter name to check in context.
- **\_typ: Type**: the expected type of the evaluated value.

Additionally `Param` accepts an optional argument:

- **strict: Boolean**: disable [flexible match](../../syntax/types/index.md#flexible-type-acceptance) (default is false).

> `Using` has this flag set as `false`, and `UsingStrict` has it set as `true`

### Param

The `Param` type provides mechanisms for checking the presence and type of parameters in the execution context.

#### Prototype members

| Name | Signature | Brief                                         |
| ---- | --------- | --------------------------------------------- |
| get  | <> Any    | Retrieve the parameter if it matches the type |

> the `get` method throws `KeyError` if the parameter is not defined, and `TypeError` if the type does not match

#### Example

```
_ <- fat.extra.Param
_ <- fat.type.Text  # the desired type must be loaded

currentUser = Param('userId', Text)

...

# Assuming userId is defined in the context and is a text,
# safely retrieve it's value from the current namespace
userId = currentUser.get
```

### Using

Apply `Using/UsingStrict` to suppress implicit parameter hints on method declarations for entries expected to be in scope.

> alternatively, to suppress warnings about implicit parameters, name the implicit entry starting with an underscore (`_`)

#### Example

```
_ <- fat.extra.Param
_ <- fat.type.Text

printUserIdFromContext = <> {
  Using('userId', Text)
  console.log(userId)
}
```

> if the implicit parameter is missing or mismatched, an error will be raised at runtime when the method is called

## See also

- [Extra package](index.md)
