{% if output.name != "ebook" %}

# Date

{% endif %}

Calendar and date handling

> operations like addition and subtraction of days, months, and years, ensuring accurate handling of various date-related complexities such as leap years and month-end calculations

## Import

```
_ <- fat.extra.Date
```

> [time library](../time.md), [math library](../math.md), [Error type](../type/error.md), [Text type](../type/text.md), [List type](../type/list.md), [Number type](../type/number.md), [Duration type](duration.md) are automatically imported with this import

## Date Type

`Date` offers a comprehensive solution for managing dates, including leap years and time of day.

### Properties

- `year`: Number - Year of the date
- `month`: Number - Month of the date
- `day`: Number - Day of the date
- `tms`: Millis - Time of the day in milliseconds

> default value points to: 1 of January of 1970

### Prototype members

| Name           | Signature                             | Brief                                     |
| -------------- | ------------------------------------- | ----------------------------------------- |
| fromEpoch      | (ems: Epoch): Date                    | Create a Date instance from an epoch time |
| isLeapYear     | (year: Number): Boolean               | Determine if a year is a leap year        |
| normalizeMonth | (month: Number): Number               | Normalize the month number                |
| daysInMonth    | (year: Number, month: Number): Number | Return number of days in month of year    |
| isValid        | (year, month, day, tms): Boolean      | Validate the date components              |
| truncate       | <> Date                               | Truncate the time of day                  |
| toEpoch        | <> Epoch                              | Convert the Date instance to epoch time   |
| addYears       | (yearsToAdd: Number): Date            | Add years to the date                     |
| addMonths      | (monthsToAdd: Number): Date           | Add months to the date                    |
| addWeeks       | (weeksToAdd: Number): Date            | Add weeks to the date                     |
| addDays        | (daysToAdd: Number): Date             | Add days to the date                      |

### Usage examples

```
_ <- fat.extra.Date

# Create a Date instance
myDate = Date(2023, 1, 1)

# Add one year to the date
newDate = myDate.addYears(1)

# Add two weeks to a date
datePlusTwoWeeks = myDate.addWeeks(2)

# Create a Date from epoch time (in milliseconds)
# result is influenced by current timezone, see: time.setZone
epochTime = 1672531200000
dateFromEpoch = Date.fromEpoch(Epoch(epochTime))

# Convert a date to epoch time
epochFromDate = myDate.toEpoch
```
