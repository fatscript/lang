{% if output.name != "ebook" %}

# extra package

{% endif %}

Additional types implemented in vanilla FatScript:

- [Date](date.md) - Calendar and date handling
- [Duration](duration.md) - Millisecond duration builder
- [Fuzzy](fuzzy.md) - Probabilistic values and fuzzy logic operations
- [HashMap](hmap.md) - Quick key-value store
- [Logger](logger.md) - Logging support
- [Memo](memo.md) - Generic memoization utility
- [MouseEvent](mouse.md) - Mouse event parser
- [Opaque](opaque.md) - Utility for soft protection of data
- [Option](option.md) - Encapsulation of optional value
- [Param](param.md) - Parameter presence and type verification
- [Sound](sound.md) - Sound playback interface
- [Storable](storable.md) - Data store facilities

## Importing

If you want to make all of them available at once you can simply write:

```
_ <- fat.extra._
```

...or import one-by-one, as needed, e.g.:

```
_ <- fat.extra.Date
```
