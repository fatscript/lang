{% if output.name != "ebook" %}

# file

{% endif %}

File input and output operations

## Import

```
_ <- fat.file
```

## Type contributions

| Name     | Signature                    | Brief         |
| -------- | ---------------------------- | ------------- |
| FileInfo | (modTime: Epoch, size: Text) | File metadata |

## Methods

| Name     | Signature                   | Brief                                |
| -------- | --------------------------- | ------------------------------------ |
| basePath | <> Text                     | Extract path where app was called    |
| resolve  | (path: Text): Text          | Return canonical name for path       |
| exists   | (path: Text): Boolean       | Check file exists on provided path   |
| read     | (path: Text): Text          | Read file from path (text mode)      |
| readBin  | (path: Text): Chunk         | Read file from path (binary mode)    |
| readSys  | (path: Text): Text          | Read system/virtual file from path   |
| write    | (path: Text, src): Void     | Write src to file and return success |
| append   | (path: Text, src): Void     | Append to file and return success    |
| remove   | (path: Text): Void          | Remove files and directories         |
| isDir    | (path: Text): Boolean       | Check if path is a directory         |
| mkDir    | (path: Text, safe: Boolean) | Create a directory                   |
| lsDir    | (path: Text): List/Text     | Get list of files in a directory     |
| stat     | (path: Text): FileInfo      | Get file metadata                    |

> starting with version `3.3.0`, in case of an exception, all methods in the `file` library raise `FileError` instead of returning a boolean or null value, providing a more consistent interface with the other standard libraries

## Usage notes

### write/append

These methods will intelligently handle different data types to optimize file output. For the `Chunk` type, they automatically write in binary mode, and for the `Text` type, as plain text. For other types, they implicitly stringify the `src` value before writing, ensuring all values are handled gracefully.

### remove

The behavior is similar to `rm -r`, removing files and directories recursively.

> starting with version `3.0.1`, symbolic links are not followed; in version `3.0.0`, symbolic links were followed; previous versions of `fry` did not implement recursive deletion

### mkDir

The behavior is similar to `mkdir -p`, creating intermediate directories when necessary.

If `safe` is set to `true`, the new directory is assigned 0700 permissions, offering more protection, instead of the default 0755 permissions, which offer less protection.

### read vs. readSys

The `read` method is optimized for reading regular files with predictable sizes, using `stat` to allocate memory efficiently before reading the entire file. In contrast, `readSys` is designed for system or virtual files from directories like `/proc` or `/sys`, where file sizes cannot be determined beforehand. It adjusts memory allocation dynamically during reading.

## See also

- [Failure library](failure.md)
- [Recode library](recode.md)
