{% if output.name != "ebook" %}

# Method

{% endif %}

Method prototype extensions

## Import

```
_ <- fat.type.Method
```

## Alias

- [Procedure](../../syntax/types/method.md#procedures): an argument-free function that executes automatically when referenced

## Constructor

| Name   | Signature  | Brief                |
| ------ | ---------- | -------------------- |
| Method | (val: Any) | Wrap val in a method |

## Prototype members

| Name     | Signature  | Brief                        |
| -------- | ---------- | ---------------------------- |
| isEmpty  | <> Boolean | Return false, always         |
| nonEmpty | <> Boolean | Return true, always          |
| size     | <> Number  | Return 1, always             |
| toText   | <> Text    | Return 'Method' text literal |
| freeze   | <> Void    | Make the value immutable     |
| arity    | <> Number  | Return method arity          |

## See also

- [Method (syntax)](../../syntax/types/method.md)
- [Type package](index.md)
