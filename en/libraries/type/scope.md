{% if output.name != "ebook" %}

# Scope

{% endif %}

Scope prototype extensions

## Import

```
_ <- fat.type.Scope
```

## Alias

Keyset: a list of keys (List/Text)

## Constructor

| Name  | Signature  | Brief                 |
| ----- | ---------- | --------------------- |
| Scope | (val: Any) | Wrap val into a scope |

## Prototype members

| Name     | Signature             | Brief                        |
| -------- | --------------------- | ---------------------------- |
| isEmpty  | <> Boolean            | Return true if size is zero  |
| nonEmpty | <> Boolean            | Return true if non-zero size |
| size     | <> Number             | Return number of entries     |
| toText   | <> Text               | Return 'Scope' text literal  |
| freeze   | <> Void               | Make the value immutable     |
| seal     | <> Void               | Prevents scope growth        |
| isSealed | <> Boolean            | Check if scope is sealed     |
| copy     | <> Scope              | Return deep copy of scope    |
| keys     | <> Keyset             | Return list of scope keys    |
| valuesOf | (t: Type): List       | Get values matching type t   |
| pick     | (keys: Keyset): Scope | Filter scope by keys         |
| omit     | (keys: Keyset): Scope | Filter scope removing keys   |
| vmap     | (m: Method): Scope    | Map values (m = v1 -> v2)    |
| maybe    | (key: Text): Option   | Return Option wrapped value  |

### Example

```
_ <- fat.type.Scope
x = { num = 12, prop = 'other' }
x.size  # yields 2
```

## See also

- [Scope (syntax)](../../syntax/types/scope.md)
- [Option type](../extra/option.md)
- [Type package](index.md)
