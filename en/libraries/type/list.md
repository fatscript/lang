{% if output.name != "ebook" %}

# List

{% endif %}

List prototype extensions

## Import

```
_ <- fat.type.List
```

## Constructor

| Name | Signature  | Brief                |
| ---- | ---------- | -------------------- |
| List | (val: Any) | Wrap val into a list |

> the constructor takes a single argument and wraps it into a list, if multiple arguments are passed, only the first argument is considered, and the rest are ignored; which is consistent with FatScript's [arguments handling](../../syntax/types/method.md#arguments-handling) support

## Prototype members

| Name       | Signature                  | Brief                                 |
| ---------- | -------------------------- | ------------------------------------- |
| isEmpty    | <> Boolean                 | Return true if length is zero         |
| nonEmpty   | <> Boolean                 | Return true if length is non-zero     |
| size       | <> Number                  | Return list length                    |
| toText     | <> Text                    | Return 'List' as text literal         |
| freeze     | <> Void                    | Make the value immutable              |
| join       | (sep: Text): Text          | Join list with separator into text    |
| flatten    | <> List                    | Flatten list of lists into one list   |
| find       | (p: Method): Any           | Return first matching item or null    |
| contains   | (p: Method): Boolean       | Check if list has match for predicate |
| filter     | (p: Method): List          | Return sub-list matching predicate    |
| reverse    | <> List                    | Return a reversed copy of the list    |
| shuffle    | <> List                    | Return a shuffled copy of the list    |
| unique     | <> List                    | Return a list of unique items         |
| sort       | <> List                    | Return a sorted copy of the list      |
| sortBy     | (key: Any): List           | Return a sorted copy of the list \*   |
| indexOf    | (item: Any): Number        | Return item index, -1 if absent       |
| head       | <> Any                     | Return first item, null if empty      |
| tail       | <> List                    | Return all items, but the first       |
| map        | (m: Method): List          | Functional utility (allows chaining)  |
| reduce     | (m: Method, acc: Any): Any | Functional utility                    |
| walk       | (m: Method): Void          | Apply side-effects to each item       |
| patch      | (i, n, val: List): List    | Insert val at i, removing n items     |
| headOption | <> Option                  | Return first item, as Option          |
| itemOption | (index: Number): Option    | Get item by index, as Option          |
| findOption | (p: Method): Option        | Search item by predicate, as Option   |

### Example

```
_ <- fat.type.List
x = [ 'a', 'b', 'c' ]
x.size  # yields 3
```

### Sorting

The `sort` and `sortBy` methods implement the quicksort algorithm, enhanced with random pivot selection. This approach is known for its efficiency, offering an average-case time complexity of O(n log n). It demonstrates high performance across most datasets. For datasets containing duplicate values or keys, stable sorting cannot be guaranteed, and performance may degrade to O(n^2) in the worst case, where all elements are identical or have the same key.

> `sortBy` accepts a textual parameter for `key` if it is a list of `Scope`, or a numerical parameter if it is a list of `List` (matrix), representing the index

### Reducing

The `reduce` method in FatScript transforms a list into a single value by applying a reducer (`m: Method`) to each element in sequence, starting from an initial accumulator value (`acc: Any`), or from the first element if no value is provided. This method is useful for operations that involve aggregating data from a list.

#### Characteristics

- **Reducer Method:** The reducer should take the current accumulator value and the current list item, returning the updated accumulator value.

- **Empty List Behavior:** When `reduce` is applied to an empty list without an initial accumulator value, it returns `null`.

> restriction: the reducer method will be considered invalid if it does not take two parameters or in case it defines default values for them

#### Practical example

```
_ <- fat.type.List
sumReducer = (acc: Number, item: Number) -> acc + item
sum = [1, 2, 3].reduce(sumReducer)  # yields 6
```

> for complex data transformations or when dealing with lists of scopes, carefully structure the reducer to handle the specific data types and desired output

## See also

- [List (syntax)](../../syntax/types/list.md)
- [Option type](../extra/option.md)
- [Type package](index.md)
