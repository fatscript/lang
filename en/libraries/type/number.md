{% if output.name != "ebook" %}

# Number

{% endif %}

Number prototype extensions

## Import

```
_ <- fat.type.Number
```

## Aliases

- Epoch: unix epoch time in milliseconds
- ExitCode: exit status or return code
- Millis: duration in milliseconds

## Constructor

| Name   | Signature  | Brief                             |
| ------ | ---------- | --------------------------------- |
| Number | (val: Any) | Text to number or collection size |

> performs the conversion from text to number assuming decimal base

## Prototype members

| Name     | Signature         | Brief                                   |
| -------- | ----------------- | --------------------------------------- |
| isEmpty  | <> Boolean        | Return true if zero                     |
| nonEmpty | <> Boolean        | Return true if non-zero                 |
| size     | <> Number         | Return absolute value, same as math.abs |
| toText   | <> Text           | Return number as text                   |
| freeze   | <> Void           | Make the value immutable                |
| format   | (fmt: Text): Text | Return number as formatted text         |
| truncate | <> Number         | Return number discarding decimals       |

### Example

```
_ <- fat.type.Number
x = Number('52')  # number: 52
x.toText          # text: '52'
x.format('.2')    # text: '52.00'
```

#### format

The `format` method is used to convert numbers into strings in various ways. The basic structure of a format specifier is `%[flags][width][.precision][type]`. Here's what each of these components mean:

- `flags` are optional characters that control specific formatting behavior. For example, `0` can be used for zero-padding and `-` for left-justification.

- `width` is an integer that specifies the minimum number of characters to be printed. If the value to be printed is shorter than this number, the result is padded with blank spaces or zeros, depending on the flag used.

- `precision` is an optional number following a `.` that specifies the number of digits to be printed after the decimal point.

- `type` is a character that specifies how the number should be represented. The common types are `f` (fixed-point notation), `e` (exponential notation), `g` (either fixed or exponential depending on the magnitude of the number), and `a` (hexadecimal floating-point notation).

Examples:

- `%5.f`: This will print the number with a total width of 5 characters, with no digits after the decimal point (because the precision is `f`, which means fixed-point, but no number follows the dot). It will be right-justified because no `-` flag is used.

- `%05.f`: Similar to the above, but because the `0` flag is used, the empty spaces will be filled with zeros.

- `%8.2f`: This will print the number with a total width of 8 characters, with 2 digits after the decimal point.

- `%-8.2f`: Similar to the above, but the number will be left-justified because of the `-` flag.

- `%.2e`: This will print the number using exponential notation, with 2 digits after the decimal point.

- `%.2a`: This will print the number using hexadecimal floating-point notation, with 2 digits after the hexadecimal point.

- `%.2g`: This will print the number in either fixed-point or exponential notation, depending on its magnitude, with a maximum of 2 significant digits.

> if the `%` symbol is not present, `fmt` is automatically evaluated as `%<fmt>f`

## See also

- [Number (syntax)](../../syntax/types/number.md)
- [Math library](../math.md)
- [Type package](index.md)
