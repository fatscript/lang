{% if output.name != "ebook" %}

# Text

{% endif %}

Text prototype extensions

## Import

```
_ <- fat.type.Text
```

## Constructor

| Name | Signature  | Brief                                  |
| ---- | ---------- | -------------------------------------- |
| Text | (val: Any) | Coerces value to text, same as .toText |

## Prototype members

| Name       | Signature                       | Brief                              |
| ---------- | ------------------------------- | ---------------------------------- |
| isEmpty    | <> Boolean                      | Return true if length is zero      |
| nonEmpty   | <> Boolean                      | Return true if non-zero length     |
| size       | <> Number                       | Return text length                 |
| toText     | <> Text                         | Return self value                  |
| freeze     | <> Void                         | Make the value immutable           |
| replace    | (old: Text, new: Text): Text    | Replace old with new (all)         |
| indexOf    | (frag: Text): Number            | Get fragment index, -1 if absent   |
| contains   | (frag: Text): Boolean           | Check if text contains fragment    |
| count      | (frag: Text): Number            | Get repetition count for fragment  |
| startsWith | (frag: Text): Boolean           | Check if starts with fragment      |
| endsWith   | (frag: Text): Boolean           | Check if ends with fragment        |
| split      | (sep: Text): List/Text          | Split text by sep into list        |
| toLower    | <> Text                         | Return lowercase version of text   |
| toUpper    | <> Text                         | Return uppercase version of text   |
| trim       | <> Text                         | Return trimmed version of text     |
| isBlank    | <> Boolean                      | Return true if only whitespaces    |
| match      | (re: Text): Boolean             | Return text is match for regex     |
| groups     | (re: Text): Scope               | Return matched regex groups        |
| repeat     | (n: Number): Text               | Return text repeated n times       |
| overlay    | (base: Text, align: Text): Text | Return text overlaid on base       |
| patch      | (i, n, val: Text): Text         | Inserts val at i, removing n chars |
| toChunk    | <> Chunk                        | Encodes to binary representation   |

### Example

```
_ <- fat.type.Text
x = 'banana'
x.size                       # yields 6
x.replace('nana', 'nquet');  # yields 'banquet'
```

### regex

When defining regular expressions, prefer to use [raw texts](../../syntax/types/text.md#raw-texts) and remember to escape backslashes as needed, ensuring that the regular expressions are interpreted correctly:

```
alphaOnly = "^[[:alpha:]]+$"
'abc'.match(alphaOnly) == true

ipAddress = "^([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})$"
'192.168.1.2'.groups(ipAddress) == {
  _0 = '192.168.1.2'
  _1 = '192'
  _2 = '168'
  _3 = '1'
  _4 = '2'
}
```

> the implemented dialect is [POSIX regex extended](https://en.wikibooks.org/wiki/Regular_Expressions/POSIX-Extended_Regular_Expressions)

### overlay

The default align value (if not provided) is 'left'. Other possible values are 'center' and 'right':

```
'x'.overlay('___')            # 'x__'
'x'.overlay('___', 'left')    # 'x__'
'x'.overlay('___', 'center')  # '_x_'
'x'.overlay('___', 'right')   # '__x'
```

> the outcome is always the same size as `base` parameter, the text will be cut if it is longer

## See also

- [Text (syntax)](../../syntax/types/text.md)
- [Type package](index.md)
