{% if output.name != "ebook" %}

# Boolean

{% endif %}

Boolean prototype extensions

## Import

```
_ <- fat.type.Boolean
```

## Constructor

| Name    | Signature  | Brief                   |
| ------- | ---------- | ----------------------- |
| Boolean | (val: Any) | Coerce value to boolean |

## Prototype members

| Name     | Signature  | Brief                            |
| -------- | ---------- | -------------------------------- |
| isEmpty  | <> Boolean | Return true if false             |
| nonEmpty | <> Boolean | Return false if true             |
| size     | <> Number  | Return 1 if true, 0 if false     |
| toText   | <> Text    | Return 'true' or 'false' as text |
| freeze   | <> Void    | Make the value immutable         |

### Examples

```
_ <- fat.type.Boolean

~ x = true
x.isEmpty       # false, since x is true
x.nonEmpty      # true, since x is not empty
x.size          # 1, since true maps to size 1
x.toText        # 'true', converts boolean to text

x.freeze
x = false       # raises an error, x is immutable after freeze

Boolean('false')  # yields true, because text is non-empty
Boolean('')       # yields false, because text is empty
```

> note that the constructor does not attempt to convert value from text, which is consistent with flow control evaluations, and you can use a simple [case](../../syntax/flow.md#cases) if you need to make conversion from text to boolean

## See also

- [Boolean (syntax)](../../syntax/types/boolean.md)
- [Type package](index.md)
