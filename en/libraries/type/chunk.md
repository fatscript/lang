{% if output.name != "ebook" %}

# Chunk

{% endif %}

Chunk prototype extensions

## Import

```
_ <- fat.type.Chunk
```

## Alias

- ByteArray: a sequence of bytes (Number [0-255])

## Constructor

| Name  | Signature  | Brief                          |
| ----- | ---------- | ------------------------------ |
| Chunk | (val: Any) | Coerce value to chunk (binary) |

> if the value is of type `Number`, creates a block of _n_ bytes initialized to zero, where _n_ is the provided number

## Prototype members

| Name      | Signature                                  | Brief                             |
| --------- | ------------------------------------------ | --------------------------------- |
| isEmpty   | <> Boolean                                 | Return true if size is zero       |
| nonEmpty  | <> Boolean                                 | Return true if non-zero size      |
| size      | <> Number                                  | Return chunk size (in bytes)      |
| toText    | <> Text                                    | Convert chunk to text format      |
| freeze    | <> Void                                    | Make the value immutable          |
| toBytes   | <> ByteArray                               | Convert chunk to bytes list       |
| toHugeInt | <> HugeInt                                 | Build HugeInt from binary data    |
| seek      | (frag: Chunk, offset: Number = 0): Number  | Return index of first match       |
| seekByte  | (byte: Number, offset: Number = 0): Number | Return index of first match       |
| patch     | (i, n, val: Chunk): Chunk                  | Insert val at i, removing n bytes |
| fit       | (len: Number): Chunk                       | Tuncate to a fixed lenght         |

> `toText` replaces any invalid UTF-8 sequences with U+FFFD, represented as � in UTF-8

### Example

```
_ <- fat.type.Chunk

# Creating a chunk from text
x = Chunk('example')

x.size     # 7, the size in bytes
x.toText   # 'example', represented as text
x.toBytes  # [ 101, 120, 97, 109, 112, 108, 101 ], the UTF-8 values

x.seek(Chunk('am'))         # 2, the position of the match
x.patch(1, 5, Chunk('XY'))  # a new chunk 'eXYe'

# Creating a chunk from a number
y = Chunk(5)  # creates a chunk of 5 bytes initialized to zero

y.size     # returns 5
y.toBytes  # returns [ 0, 0, 0, 0, 0 ]
```

## See also

- [Chunk (syntax)](../../syntax/types/chunk.md)
- [Type package](index.md)
