{% if output.name != "ebook" %}

# Error

{% endif %}

Error prototype extensions

## Import

```
_ <- fat.type.Error
```

## Aliases

- AssignError: assigning a new value to an immutable entry
- AsyncError: asynchronous operation failure
- CallError: a call is made with insufficient arguments
- FileError: file operation failure
- IndexError: index is out of list/text bounds
- KeyError: the key (name) is not found in scope
- SyntaxError: syntax or code structure error
- TypeError: type mismatch on method call, return, or assign
- ValueError: type may be okay, but content is not accepted

## Constructor

| Name  | Signature  | Brief                                       |
| ----- | ---------- | ------------------------------------------- |
| Error | (val: Any) | Return val coerced to text wrapped in error |

## Prototype members

| Name     | Signature  | Brief                    |
| -------- | ---------- | ------------------------ |
| isEmpty  | <> Boolean | Return true, always      |
| nonEmpty | <> Boolean | Return false, always     |
| size     | <> Number  | Return 0, always         |
| toText   | <> Text    | Return error text val    |
| freeze   | <> Void    | Make the value immutable |

### Example

```
_ <- fat.type.Error

# Generating an error intentionally
x = Error('ops')
x.toText  # yields "Error: ops"

# Inadvertently causing an error
e = undeclared.item  # causes a TypeError
e.toText             # yields "TypeError: can't resolve scope of 'item'"
```

### Error aliases in practice

```
# Example of AssignError
x = 10
x = 20  # raises "AssignError: reassignment to immutable > x"

# Example of IndexError
list = [ 1, 2, 3 ]
list[5]  # raises "IndexError: out of bounds"

# Example of CallError
add(10)  # raises "CallError: nothing to call > add > ..."
```

## See also

- [Failure library](../failure.md)
- [Error (syntax)](../../syntax/types/error.md)
- [Type package](index.md)
