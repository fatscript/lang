{% if output.name != "ebook" %}

# HugeInt

{% endif %}

HugeInt prototype extensions

## Import

```
_ <- fat.type.HugeInt
```

## Constructor

| Name    | Signature  | Brief                          |
| ------- | ---------- | ------------------------------ |
| HugeInt | (val: Any) | Parse number or text to HugInt |

> when converting from `Text`, the input is interpreted as a hexadecimal representation

## Prototype members

| Name     | Signature                             | Brief                              |
| -------- | ------------------------------------- | ---------------------------------- |
| isEmpty  | <> Boolean                            | Return true if zero                |
| nonEmpty | <> Boolean                            | Return true if non-zero            |
| size     | <> Number                             | Return number of bits to represent |
| toText   | <> Text                               | Return number as hexadecimal text  |
| freeze   | <> Void                               | Make the value immutable           |
| modExp   | (exp: HugeInt, mod: HugeInt): HugeInt | Return modular exponentiation      |
| toNumber | <> Number                             | Converts to number (with loss)     |
| toChunk  | <> Chunk                              | Encodes to binary representation   |

### Examples

```
_ <- fat.type.HugeInt

# Converting HugeInt
x = 0x1f4   # 500 in hexadecimal
x.toText    # returns '1f4'
x.toNumber  # returns 500
x.size      # 9 bits are required to represent 500

# Modular exponentiation
y = 0x3           # 3 in hexadecimal
z = 0x5           # 5 in hexadecimal
mod = 0x7         # 7 in hexadecimal
y.modExp(z, mod)  # 0x5, equivalent to (3^5) % 7
```

### Usage notes

#### Conversion from Number to HugeInt

- The maximum allowed value for `Number` conversion is `2^53`.
- Attempting to pass a value greater than `2^53` will raise a `ValueError`.

#### Conversion from HugeInt to Number

- Values up to `2^1023 - 1` can be converted, though some precision loss may occur for very large values.
- If the value exceeds this limit, the result will be `infinity`. This can be verified using the `isInf` method from the [math library](../math.md).

> the math library also provides the `maxInt` value, which serves to assess potential precision loss; if a number is less than `maxInt`, its conversion from `HugeInt` is considered safe without precision loss

## See also

- [HugeInt (syntax)](../../syntax/types/hugeint.md)
- [Type package](index.md)
