{% if output.name != "ebook" %}

# smtp

{% endif %}

SMTP handling framework

> this library provides a simple interface for configuring SMTP parameters and sending emails

## Import

```
_ <- fat.smtp
```

## Types

The `smtp` library introduces the `ContactInfo` type.

### ContactInfo

The `ContactInfo` type represents an email contact, which can include an optional name along with the email address.

#### Constructor

| Name        | Signature                      | Brief                          |
| ----------- | ------------------------------ | ------------------------------ |
| ContactInfo | (email: Text, name: Text = '') | Construct a ContactInfo object |

- **email**: The email address of the contact.
- **name** (optional): The name of the contact.

## Methods

### config

Configures the SMTP settings.

| Parameter | Type        | Description                           |
| --------- | ----------- | ------------------------------------- |
| from      | ContactInfo | An object representing the sender.    |
| server    | Text        | The SMTP server URL/address.          |
| username  | Text        | The username for SMTP authentication. |
| password  | Text        | The password for SMTP authentication. |
| useSSL    | Boolean     | Use SSL/TLS (defaults to true).       |

> raises an error if the configuration fails

### send

Sends an email.

| Parameter | Type             | Description               |
| --------- | ---------------- | ------------------------- |
| to        | List/ContactInfo | A list of recipients.     |
| subject   | Text             | The subject of the email. |
| body      | Text             | The body of the email.    |

> returns the message UUID on success

## Usage notes

Example:

```
smtp <- fat.smtp

smtp.config(
  from = ContactInfo('sender@example.com', 'Sender Name')
  server = 'smtps://smtp.example.com:port'
  username = 'your_username'
  password = 'your_password'
)

smtp.send(
  to = [
    ContactInfo('recipient1@example.com', 'Recipient One')
    ContactInfo('recipient2@example.com')  # name is optional
  ]
  subject = 'Test Email'
  body = 'This is a test email sent using fat.smtp.'
)
```

SSL/TLS is enabled by default in the SMTP configuration. If your SMTP server requires SSL/TLS, no additional configuration is needed. However, if your server does not support SSL/TLS, you can disable it by setting `useSSL` to `false` when calling `config`.

### SMTP in Web Build

When using `fry` built with Emscripten (for example, when using [FatScript Playground](https://fatscript.org/playground)), there is no support for this library.
