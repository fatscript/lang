![FatScript logo](../logo/fatscript-ascii-art.png)

## Hello World

```
_ <- fat.std
console.log('Hello World')
```

## Quick Start

Jump straight into the docs:

- [General overview](general/index.md)
- [Language syntax](syntax/index.md)
- [Standard libraries](libraries/index.md)

## Fry Interpreter

For local execution, use the `fry` interpreter. It's free and open source! You can find the code, examples, and more on our [GitLab repository](https://gitlab.com/fatscript/fry).

For details on its installation and usage, refer to the [setup](general/setup.md) section.

## Web Playground

For quick and convenient testing, run your code directly in the [FatScript Playground](https://fatscript.org/playground). The playground features a web-based REPL with an intuitive interface that allows you to load scripts from a file, facilitating swift experimentation.

## Tutorials

Dive into our immersive tutorials, behind-the-scenes insights, and surrounding topics in the [FatScript YouTube channel](https://www.youtube.com/@fatscript).

## PDF Download

- [FatScript v4.1.0 (current)](../pdf/fatscript_v4_en.pdf)
- [FatScript v3.4.0 (legacy)](../pdf/fatscript_v3_en.pdf)
- [FatScript v2.6.0 (legacy)](../pdf/fatscript_v2_en.pdf)
- [FatScript v1.3.5 (legacy)](../pdf/fatscript_v1_en.pdf)

## Donations

Did you find FatScript useful and would like to say thanks?

[Buy me a coffee](https://www.buymeacoffee.com/aprates)

## License

[GPLv3](../LICENSE) © 2022-2025 Antonio Prates

[fatscript.org](https://fatscript.org)

---

Published on {{ honkit.time }}
