{% if output.name != "ebook" %}

# Loops

{% endif %}

Repeat, repeat, repeat, repeat, repeat...

## Base syntax

All loops are build with an "at" sign `@`, like so:

```
<expression> @ <loopBody>
```

### While loop

The loop body will execute while the expression evaluates to:

- true
- non-zero number
- non-empty text/chunk

The execution will terminate when the expression evaluates to:

- false
- null
- zero number
- empty text/chunk
- error

For example, this loop prints numbers 0 to 3:

```
_ <- fat.console

~ i = 0

(i < 4) @ {
  log(i)
  i += 1
}
```

## Mapping syntax

You can map over ranges, lists and scopes with a mapper, like so:

```
<range|collection> @ <mapper>
```

A new list is generated based from the return values of the mapper.

### Mapping over a range

Using range operator `..` the mapper will receive a number as input sequentially from the left bound to the right bound:

```
4..0 @ num -> num + 1  # returns [ 5, 4, 3, 2, 1 ]
```

> range syntax is inclusive on both sides, e.g. 0..2 yields 0, 1, 2

There is also half-open range operator `..<` exclusive on the right-hand side.

> caveat: half-open range won't work with reverse direction, always needs to be from the minimum to maximum

### Mapping over a list

The mapper will receive items in order (from left to right):

```
[ 3, 1, 2 ] @ item -> item + 1  # returns [ 4, 2, 3 ]
```

### Mapping over a scope

The mapper will receive the names (keys) of the entries stored in the scope in alphabetical order:

```
{ c = 3, a = 1, b = 2 } @ key -> key  # yields [ 'a', 'b', 'c' ]
```

> on the examples we have used list and scope literals, but an entry or call that evaluates to a list or a scope will have the same effect

To access entries in a scope, you refer to it by name, but in this case, it needs to be defined in the outer scope, for example:

```
myScope = { c = 3, a = 1, b = 2 }
myScope @ key -> myScope(key)  # returns [ 1, 2, 3 ]
```

FatScript uses an intelligent caching feature that prevents this syntax from generating additional effort to search for the current element in the scope while mapping.
