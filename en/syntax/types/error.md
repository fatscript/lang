{% if output.name != "ebook" %}

# Error

{% endif %}

There is great wisdom in expecting the unexpected too.

## Default subtypes

While some errors may be raised with the base Error type, most are [subtyped](index.md#type-alias).

See the definitions in the [error prototype extensions](../../libraries/type/error.md).

## Declaration

Errors can also be raised explicitly; you must use the [type constructor](../../libraries/type/error.md):

```
_ <- fat.type.Error

Error('an error has ocurred')  # raises a generic error

MyMistake = Error
MyMistake('another error has ocurred')  # raises a MyMistake subtype error
```

## Comparisons

Errors always evaluate as falsy:

```
Error() ? 'is truthy' : 'is falsy'  # is false
```

Errors are comparable to their type:

```
Error() == Error  # true
```

> read also about [type comparison](index.md#checking-types) syntax

A naive way of handling errors could be:

```
_ <- fat.console
# handling the returned error
maybeFail() <= Error => log('an error has happened')
_                    => log('success')
```

> this only works if [option](../../general/options.md) `-e / continue on error` is set

Another naive way to deal with errors, but one that always works, is to use a [default operation](../flow.md#fallback):

```
maybeFail() ?? log('an error occurred')
```

Although the naive approach may work, the proper way to deal with errors is by setting an error handler using the `trapWith` method found in the [failure library](../../libraries/failure.md).

## See also

- [Failure library](../../libraries/failure.md)
- [Error prototype extensions](../../libraries/type/error.md)
