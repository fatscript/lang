{% if output.name != "ebook" %}

# Text

{% endif %}

Texts can hold many characters, and are sometimes referred to as strings.

## Declaration

Text entries are declared using quotes:

```
a = 'hello world'        # smart text declaration
a = "hello world"        # raw text declaration
a: Text = 'hello world'  # smart, optionally verbose
```

## Manipulating text

### Concatenation

In FatScript, you can concatenate, or join, two texts using the `+` operator. This operation connects the two texts into one. For example:

```
x1 = 'ab' + 'cd'  # Outputs 'abcd'
```

### Text subtraction

FatScript also supports a text subtraction operation using the `-` operator. This operation removes a specified substring from the text. For instance:

```
x2 = 'ab cd'
x2 - ' ' == 'abcd'  # Outputs true
```

In the above example, the space character `' '` is removed from the original text `'ab cd'`, resulting in `'abcd'`.

### Text selection

Selection allows you to access specific parts of a text using indices. In FatScript, you can use either positive or negative indices. Positive indices start from the beginning of the text (`0` is the first character), and negative indices start from the end of the text (`-1` is the last character).

> for detailed explanation about the indexing system in FatScript, refer to the section on accessing and selecting items in [List](list.md)

When only one index is passed to the selection function, a single character from the text is selected. When a range is passed to the function, a fragment from the text is selected. This selection is inclusive, meaning that it includes the characters at both the start and end indices, unless using half-open range operator `..<` exclusive on the right-hand side.

Like with lists, accessing items that are out of valid indices will generate an error. For selections, no errors are generated when accessing out-of-bounds indices; instead, an empty text is returned.

```
x3 = 'example'
x3(1)     # 'x'
x3(2, 4)  # 'amp'
x3(..2)   # 'exa'
x3(..<2)  # 'ex'
```

### Direct manipulation

Text mutation allows altering specific characters by position. For example, in `~ text = 'ai'`, you can modify the last character with `text[1] = 'e'`, transforming the text into `'ae'`.

## Special characters

Characters such as quotes `'` / `"` can be escaped with backslash `\`.

```
'Rock\'n\'roll'
"Where is \"here\"?"
```

> you only need to escape quotes of same type used as text delimiter

Other supported escape sequences are are:

- backspace `\b`
- new line `\n`
- carriage return `\r`
- tab `\t`
- escape `\e`
- octet in base-8 representation `\ooo`
- octet in hexadecimal representation `\xhh`
- backslash itself `\\`

## Smart texts

When declared with single quotes `'`, the smart mode is enabled, and interpolation is performed for any code wrapped in curly brackets `{...}`:

```
text = 'world'
interpolated = 'hello {text}'  # outputs 'hello world'
```

> the template is processed in a layer with access to current scope

Note that the use of new lines or other smart texts inside the interpolation template code is not supported, but you can make method calls if you need to compose the result with something more complex.

You can avoid interpolation by escaping the opening bracket:

```
escaped = 'hello \{text}'  # outputs 'hello {text}'
```

Alternatively, you can avoid interpolation by using raw texts.

## Raw texts

When declared with double quotes `"` the raw text mode is assumed and interpolation is disabled.

Smart mode vs. raw mode example:

```
'I am smart: {interpolated}'  # using value from previous example
I am smart: hello world       # replacement occurs

"I am raw: {interpolated}"  # brackets are just common characters
I am raw: {interpolated}    # no interpolation occurs
```

## Operating texts

- `==` equal
- `!=` not equal
- `+` plus (concatenate)
- `-` minus (removes substring)
- `<` less (alphanumeric)
- `<=` less or equal (alphanumeric)
- `>` more (alphanumeric)
- `>=` more or equal (alphanumeric)
- `&` logical AND (coerced to boolean)
- `|` logical OR (coerced to boolean)

## Encoding

FatScript is designed to operate with text encoded in UTF-8. This design choice acknowledges the prevalence of these encoding systems and optimizes the language for broad compatibility.

UTF-8 is a multi-byte encoding system capable of representing any character in the Unicode standard. This universal character encoding scheme uses 8 to 32 bits to represent a character, enabling the depiction of a vast array of symbols from numerous languages and writing systems. Notably, the first 128 characters (0-127) of UTF-8 align precisely with the ASCII set, making any ASCII text a valid UTF-8 encoded string.

In FatScript, the Text data type is a sequence of Unicode characters, inherently encoded in UTF-8, therefore operations such as `text.size`, `text(index)`, and `text(1..4)` will correctly count, access, or slice text irrespective of the complexity of the characters. These operations consider a complete multi-byte UTF-8 character as a single unit, ensuring correct and predictable behavior.

## See also

- [Text prototype extensions](../../libraries/type/text.md)
