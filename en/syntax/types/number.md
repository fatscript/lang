{% if output.name != "ebook" %}

# Number

{% endif %}

A mathematical concept used to count, measure and do other [maths](../../libraries/math.md) stuff.

## Declaration

The `Number` type is implemented as `double`. Here's how to declare a number:

```
a = 5              # number declaration (immutable)
b: Number = 5      # same effect, with type-checking
c: Number = a      # initiating from entry value, also 5
d = 43.14          # with decimals
```

To declare a mutable entry, prepend it with the tilde operator:

```
~ a = 6  # mutable number entry
a += 1   # adds 1 to 'a', yields 7
```

## Operating numbers

Numbers accept quite a few operations:

- `==` equal
- `!=` not equal
- `+` plus
- `-` minus
- `*` multiply
- `/` divide
- `%` modulus
- `**` power
- `<` less
- `<=` less or equal
- `>` more
- `>=` more or equal
- `&` logical AND
- `|` logical OR

### Caveats

For logical operations and flow control, keep in mind that zero is falsy and non-zero is truthy.

For equality operators, although `0` and `null` are evaluated as falsy, in FatScript they are not the same:

```
0 == null  # false
```

## Precision

Although the arithmetic precision of a `IEEE 754 double` is higher, `fry` employs rounding tricks to improve human readability when printing long decimal sequences as text. Additionally, it uses an epsilon of `1.0e-06` for 'equality' comparisons between numbers.

In 99.999% of use cases, this approach provides both more convenient comparisons and more natural-looking numbers:

```
# Equality epsilon
x = 1.0e-06
x: Number = 0.000001

# Smaller differences are treated as the "same" number by comparison
x == 0.0000015
Boolean: true  # the 0.0000005 difference is ignored
```

Floating-point numbers aren't distributed evenly on the number line. They are dense around 0, and as the magnitude increases, the 'delta' between two expressible values increases:

```
_____________________________________0_____________________________________
+infinity    |     |    |   |  |  | ||| |  |  |   |    |     |    -infinity
```

> the biggest contiguous integer is 9,007,199,254,740,992 or 2^53

You can still have much larger numbers, around 10^308, which is:

```
100000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000
```

Bear in mind that if you add 1 to 10^308, no matter how many times you do it, it will always result in the same value! You need to add at least something near 10^293 in a single operation for it to be considered, as the numbers need to be of similar orders of magnitude. To discreetly handle numbers exceeding 2^53, consider using the [HugeInt](hugeint.md) type.

Also, the `infinity` keyword provides a clear, unambiguous representation of values that soar into the realms beyond the largest expressible numbers, approaching the theoretical infinity.

## See also

- [Number prototype extensions](../../libraries/type/number.md)
- [Math library](../../libraries/math.md)
