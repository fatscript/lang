{% if output.name != "ebook" %}

# Syntax

{% endif %}

## Essential aspects

### Imports <-

```
console <- fat.console
```

### Values (v)

Value names start with lowercase:

```
name = 'Mary'
age = 25
```

> values are constants, unless initially declared with a tilde

### Variables ~

```
~ email = 'my@email.com'
~ isOnline = true
```

### Lists []

```
list = [ 1, 2, 3 ]

list(0)        # Outputs 1, read-only
list[0]        # Outputs 1, read/write, in case list can be changed
```

### Scopes {}

```
scope = { key1 = 'value1', key2 = 'value2' }

scope.key1     # Outputs 'value1' (dot access)
scope('key1')  # Outputs 'value1', read-only (call access)
scope['key1']  # Outputs 'value1', read/write, in case value can be changed
```

### Types (T)

Type names start with uppercase:

```
Person = (name: Text, age: Number)
person = Person('Mary', 25)
```

### Methods ->

```
greeting = (name: Text): Text -> 'Hello, {name}'
console.log(greeting('World'))
```

> methods are also considered values

### Nullish coalescence ??

```
maybeValue ?? fallback    # use fallback if maybeValue is null/error
```

### If-Else \_ ? \_ : \_

```
condition ? then : else   # if condition is true, then do "then", otherwise "else"
```

### Match cases =>

```
condition1 => result1
condition2 => result2
conditionN => resultN
_          => default     # catch-all case
```

### Switch >>

```
value >> {
  match1 => result1
  match2 => result2
  matchN => resultN
  _      => default       # catch-all case
}
```

### Tap <<

```
expression << tapMethod
```

> uses tapMethod only for it's effects on the value returned by expression

### Loops @

```
condition @ loopBody              # loop while the condition is true
1..10 @ n -> rangeMapper(n)       # iterate over the range 1 to 10
list @ item -> listMapper(item)   # iterate over list items
scope @ key -> scopeMapper(key)   # iterate over scope keys
```

### Procedures <>

```
~ users = [
  { name = 'Foo', age = 30 }
  { name = 'Bar', age = 28 }
]
userNames = List <> users @ -> _.name
userNames  # Outputs ['Foo', 'Bar']
```

## Deep dive

In the following pages, you will find information on the central aspects of writing FatScript code, using both the basic language features as well as the advanced type system and standard libraries features.

- [Formatting](formatting.md): how to format FatScript code properly

- [Imports](imports.md): how to import libraries into your code

- [Entries](entries.md): understanding the concept of entries and scopes

- [Types](types/index.md): a guide to FatScript type system

  - [Any](types/any.md) - anything
  - [Void](types/void.md) - nothing
  - [Boolean](types/boolean.md) - primitive
  - [Number](types/number.md) - primitive
  - [HugeInt](types/hugeint.md) - primitive
  - [Text](types/text.md) - primitive
  - [Method](types/method.md) - function or lambda
  - [List](types/list.md) - like array or stack
  - [Scope](types/scope.md) - like object or dictionary
  - [Error](types/error.md) - yes, for errors
  - [Chunk](types/chunk.md) - binary data


- [Flow control](flow.md): controlling the program execution with conditionals

- [Loops](loops.md): making use of ranges, map-over and while loops
