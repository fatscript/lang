# Sumário

- [Visão geral](general/index.md)
  - [Configuração](general/setup.md)
  - [Opções](general/options.md)
  - [Empacotamento](general/bundling.md)
  - [Ferramentas](general/tooling.md)
- [Sintaxe](syntax/index.md)
  - [Formatação](syntax/formatting.md)
  - [Importações](syntax/imports.md)
  - [Entradas](syntax/entries.md)
  - [Tipos](syntax/types/index.md)
    - [Any](syntax/types/any.md)
    - [Void](syntax/types/void.md)
    - [Boolean](syntax/types/boolean.md)
    - [Number](syntax/types/number.md)
    - [HugeInt](syntax/types/hugeint.md)
    - [Text](syntax/types/text.md)
    - [Method](syntax/types/method.md)
    - [List](syntax/types/list.md)
    - [Scope](syntax/types/scope.md)
    - [Error](syntax/types/error.md)
    - [Chunk](syntax/types/chunk.md)
  - [Controle de fluxo](syntax/flow.md)
  - [Loops](syntax/loops.md)
- [Bibliotecas](libraries/index.md)
  - [async](libraries/async.md)
  - [bridge](libraries/bridge.md)
  - [color](libraries/color.md)
  - [console](libraries/console.md)
  - [curses](libraries/curses.md)
  - [enigma](libraries/enigma.md)
  - [failure](libraries/failure.md)
  - [file](libraries/file.md)
  - [http](libraries/http.md)
  - [math](libraries/math.md)
  - [recode](libraries/recode.md)
  - [sdk](libraries/sdk.md)
  - [smtp](libraries/smtp.md)
  - [socket](libraries/socket.md)
  - [system](libraries/system.md)
  - [time](libraries/time.md)
  - [type.\_](libraries/type/index.md)
    - [Void](libraries/type/void.md)
    - [Boolean](libraries/type/boolean.md)
    - [Number](libraries/type/number.md)
    - [HugeInt](libraries/type/hugeint.md)
    - [Text](libraries/type/text.md)
    - [Method](libraries/type/method.md)
    - [List](libraries/type/list.md)
    - [Scope](libraries/type/scope.md)
    - [Error](libraries/type/error.md)
    - [Chunk](libraries/type/chunk.md)
  - [extra.\_](libraries/extra/index.md)
    - [Date](libraries/extra/date.md)
    - [Duration](libraries/extra/duration.md)
    - [Fuzzy](libraries/extra/fuzzy.md)
    - [HashMap](libraries/extra/hmap.md)
    - [Logger](libraries/extra/logger.md)
    - [Memo](libraries/extra/memo.md)
    - [MouseEvent](libraries/extra/mouse.md)
    - [Opaque](libraries/extra/opaque.md)
    - [Option](libraries/extra/option.md)
    - [Param](libraries/extra/param.md)
    - [Sound](libraries/extra/sound.md)
    - [Storable](libraries/extra/storable.md)
  - [Comandos embutidos](libraries/embedded.md)
