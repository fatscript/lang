{% if output.name != "ebook" %}

# file

{% endif %}

Operações de entrada e saída de arquivo

## Importação

```
_ <- fat.file
```

## Contribuições de Tipo

| Nome     | Assinatura                   | Breve descrição      |
| -------- | ---------------------------- | -------------------- |
| FileInfo | (modTime: Epoch, size: Text) | Metadados do arquivo |

## Métodos

| Nome     | Assinatura                  | Breve descrição                            |
| -------- | --------------------------- | ------------------------------------------ |
| basePath | <> Text                     | Extrair caminho onde o app foi chamado     |
| resolve  | (path: Text): Text          | Retorna nome canônico do caminho           |
| exists   | (path: Text): Boolean       | Verificar se existe arquivo no caminho     |
| read     | (path: Text): Text          | Ler arquivo do caminho (modo de texto)     |
| readBin  | (path: Text): Chunk         | Ler arquivo do caminho (modo de binário)   |
| readSys  | (path: Text): Text          | Ler arquivo de sistema/virtual do caminho  |
| write    | (path: Text, src): Void     | Escrever src no arquivo e retornar sucesso |
| append   | (path: Text, src): Void     | Acrescentar ao arquivo e retornar sucesso  |
| remove   | (path: Text): Void          | Apagar arquivos e diretórios               |
| isDir    | (path: Text): Boolean       | Verificar se o caminho é um diretório      |
| mkDir    | (path: Text, safe: Boolean) | Criar um diretório                         |
| lsDir    | (path: Text): List/Text     | Obter lista de arquivos em um diretório    |
| stat     | (path: Text): FileInfo      | Obter metadados do arquivo                 |

> a partir da versão `3.3.0`, em caso de exceção, todos os métodos da biblioteca `file` lançam `FileError` no lugar de retornar um valor booleano ou nulo, oferecendo uma interface mais homogênea com as demais bibliotecas padrão

## Notas de uso

### write/append

Estes métodos tratam de forma inteligente diferentes tipos de dados para otimizar a saída de arquivos. Para o tipo `Chunk`, eles escrevem automaticamente em modo binário e para o tipo `Text`, como texto simples. Para outros tipos, eles implicitamente "stringificam" o valor `src` antes de escrever, garantindo que todos e qualquer valor seja tratado de maneira adequada.

### remove

O comportamento é semelhante a `rm -r`, removendo arquivos e diretórios de forma recursiva.

> a partir da versão `3.0.1`, links simbólicos não são seguidos; na versão `3.0.0`, os links simbólicos eram seguidos; as versões anteriores não implementavam exclusão recursiva

### mkDir

O comportamento é semelhante a `mkdir -p` criando diretórios intermediários quando necessário.

Se `safe` for definido como `true`, o novo diretório receberá permissões 0700, oferecendo mais proteção, em vez das permissões padrão 0755, que oferecem menos proteção.

### read vs. readSys

O método `read` é otimizado para a leitura de arquivos regulares com tamanhos previsíveis, utilizando `stat` para alocar memória de forma eficiente antes de ler o arquivo inteiro. Em contraste, `readSys` é projetado para arquivos de sistema ou virtuais de diretórios como `/proc` ou `/sys`, onde os tamanhos dos arquivos não podem ser determinados de antemão. Ele ajusta a alocação de memória dinamicamente durante a leitura.

## Veja também

- [Biblioteca failure](failure.md)
- [Biblioteca recode](recode.md)
