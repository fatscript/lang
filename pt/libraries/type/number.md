{% if output.name != "ebook" %}

# Number

{% endif %}

Extensões do protótipo Number

## Importação

```
_ <- fat.type.Number
```

## Aliases

- Epoch: tempo de época do unix em milissegundos
- ExitCode: status de saída ou código de retorno
- Millis: duração em milissegundos

## Construtor

| Nome   | Assinatura | Breve descrição                         |
| ------ | ---------- | --------------------------------------- |
| Number | (val: Any) | Texto para número ou tamanho da coleção |

> realiza a conversão de texto para número assumindo a base decimal

## Membros do protótipo

| Nome     | Assinatura        | Breve descrição                          |
| -------- | ----------------- | ---------------------------------------- |
| isEmpty  | <> Boolean        | Retorna verdadeiro se zero               |
| nonEmpty | <> Boolean        | Retorna verdadeiro se não-zero           |
| size     | <> Number         | Retorna valor absoluto, igual a math.abs |
| toText   | <> Text           | Retorna número como texto                |
| freeze   | <> Void           | Torna o valor imutável                   |
| format   | (fmt: Text): Text | Retorna número como texto formatado      |
| truncate | <> Number         | Retorna número descartando decimais      |

### Exemplo

```
_ <- fat.type.Number
x = Number('52')  # número: 52
x.toText          # texto: '52'
x.format('.2')    # texto: '52.00'
```

#### format

O método `format` é usado para converter números em strings de várias maneiras. A estrutura básica de um especificador de formato é `%[flags][width][.precision][type]`. Aqui está o que cada um destes componentes significa:

- `flags` são caracteres opcionais que controlam o comportamento específico de formatação. Por exemplo, `0` pode ser usado para preenchimento com zeros e `-` para justificação à esquerda.

- `width` é um número inteiro que especifica o número mínimo de caracteres a serem impressos. Se o valor a ser impresso for mais curto do que este número, o resultado é preenchido com espaços em branco ou zeros, dependendo da flag utilizada.

- `precision` é um número opcional que segue um `.` que especifica o número de dígitos a serem impressos após o ponto decimal.

- `type` é um caractere que especifica como o número deve ser representado. Os tipos comuns são `f` (notação de ponto fixo), `e` (notação exponencial), `g` (fixo ou exponencial dependendo da magnitude do número) e `a` (notação de ponto flutuante hexadecimal).

Exemplos:

- `%5.f`: Isso imprimirá o número com uma largura total de 5 caracteres, sem dígitos após o ponto decimal (porque a precisão é `f`, que significa ponto fixo, mas nenhum número segue o ponto). Será justificado à direita porque nenhuma flag `-` é usada.

- `%05.f`: Semelhante ao anterior, mas como a flag `0` é usada, os espaços vazios serão preenchidos com zeros.

- `%8.2f`: Isso imprimirá o número com uma largura total de 8 caracteres, com 2 dígitos após o ponto decimal.

- `%-8.2f`: Semelhante ao anterior, mas o número será justificado à esquerda por causa da flag `-`.

- `%.2e`: Isso imprimirá o número usando notação exponencial, com 2 dígitos após o ponto decimal.

- `%.2a`: Isso imprimirá o número usando notação de ponto flutuante hexadecimal, com 2 dígitos após o ponto hexadecimal.

- `%.2g`: Isso imprimirá o número em notação de ponto fixo ou exponencial, dependendo da sua magnitude, com no máximo 2 dígitos significativos.

> caso o `%` não esteja presente, `fmt` é automaticamente avaliado como `%<fmt>f`

## Veja também

- [Number (sintaxe)](../../syntax/types/number.md)
- [Biblioteca math](../math.md)
- [Pacote type](index.md)
