{% if output.name != "ebook" %}

# Method

{% endif %}

Extensões do protótipo Method

## Importação

```
_ <- fat.type.Method
```

## Alias

- [Procedure](../../syntax/types/method.md#procedimentos): uma função sem argumentos que executa automaticamente quando referenciada

## Construtor

| Nome   | Assinatura | Breve descrição          |
| ------ | ---------- | ------------------------ |
| Method | (val: Any) | Envolve val em um método |

## Membros do protótipo

| Nome     | Assinatura | Breve descrição                     |
| -------- | ---------- | ----------------------------------- |
| isEmpty  | <> Boolean | Retorna falso, sempre               |
| nonEmpty | <> Boolean | Retorna verdadeiro, sempre          |
| size     | <> Number  | Retorna 1, sempre                   |
| toText   | <> Text    | Retorna o literal de texto 'Method' |
| freeze   | <> Void    | Torna o valor imutável              |
| arity    | <> Number  | Retorna a aridade do método         |

## Veja também

- [Method (sintaxe)](../../syntax/types/method.md)
- [Pacote type](index.md)
