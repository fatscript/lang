{% if output.name != "ebook" %}

# Error

{% endif %}

Extensões do protótipo Error

## Importação

```
_ <- fat.type.Error
```

## Aliases

- AssignError: atribuindo um novo valor a uma entrada imutável
- AsyncError: falha na operação assíncrona
- CallError: uma chamada é feita com argumentos insuficientes
- FileError: falha na operação de arquivo
- IndexError: o índice está fora dos limites da lista/texto
- KeyError: a chave (nome) não é encontrada no escopo
- SyntaxError: erro de sintaxe ou estrutura de código
- TypeError: inconsistência de tipo em chamada, retorno ou atribuição de método
- ValueError: tipo pode estar correto, mas conteúdo não é aceito

## Construtor

| Nome  | Assinatura | Breve descrição                           |
| ----- | ---------- | ----------------------------------------- |
| Error | (val: Any) | Retornar val coagido para texto como erro |

## Membros do protótipo

| Nome     | Assinatura | Breve descrição            |
| -------- | ---------- | -------------------------- |
| isEmpty  | <> Boolean | Retorna verdadeiro, sempre |
| nonEmpty | <> Boolean | Retorna falso, sempre      |
| size     | <> Number  | Retorna 0, sempre          |
| toText   | <> Text    | Retorna o texto do erro    |
| freeze   | <> Void    | Torna o valor imutável     |

### Exemplo

```
_ <- fat.type.Error

# Gerando um erro intencionalmente
x = Error('ops')
x.toText  # retorna "Error: ops"

# Causando um erro inadvertidamente
e = undeclared.item  # causa um TypeError
e.toText             # retorna "TypeError: can't resolve scope of 'item'"
```

### Aliases de erro na prática

```
# Exemplo de AssignError
x = 10
x = 20  # gera "AssignError: reassignment to immutable > x"

# Exemplo de IndexError
list = [ 1, 2, 3 ]
list[5]  # gera "IndexError: out of bounds"

# Exemplo de CallError
add(10)  # gera "CallError: nothing to call > add > ..."
```

## Veja também

- [Biblioteca failure](../failure.md)
- [Error (sintaxe)](../../syntax/types/error.md)
- [Pacote type](index.md)
