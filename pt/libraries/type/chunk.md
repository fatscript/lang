{% if output.name != "ebook" %}

# Chunk

{% endif %}

Extensões do protótipo Chunk

## Importação

```
_ <- fat.type.Chunk
```

## Alias

- ByteArray: uma sequência de bytes (Number [0-255])

## Construtor

| Nome  | Assinatura | Breve descrição                  |
| ----- | ---------- | -------------------------------- |
| Chunk | (val: Any) | Coage valor para bloco (binário) |

> se o valor for do tipo `Number`, cria um bloco de _n_ bytes inicializados com zero, onde _n_ é o número fornecido

## Membros do protótipo

| Nome      | Assinatura                                 | Breve descrição                            |
| --------- | ------------------------------------------ | ------------------------------------------ |
| isEmpty   | <> Boolean                                 | Retorna verdadeiro se o tamanho for zero   |
| nonEmpty  | <> Boolean                                 | Retorna verdadeiro se tamanho não-zero     |
| size      | <> Number                                  | Retorna o tamanho do bloco (em bytes)      |
| toText    | <> Text                                    | Converte o bloco para formato de texto     |
| freeze    | <> Void                                    | Torna o valor imutável                     |
| toBytes   | <> ByteArray                               | Converte o bloco para uma lista de bytes   |
| toHugeInt | <> HugeInt                                 | Cria HugeInt a partir de dados binários    |
| seek      | (frag: Chunk, offset: Number = 0): Number  | Retorna índice da primeira correspondência |
| seekByte  | (byte: Number, offset: Number = 0): Number | Retorna índice da primeira correspondência |
| patch     | (i, n, val: Chunk): Chunk                  | Insere val na posição i, removendo n bytes |
| fit       | (len: Number): Chunk                       | Trunca para um comprimento fixo            |

> `toText` substitui quaisquer sequências inválidas de UTF-8 por U+FFFD, representado como � em UTF-8

### Exemplo

```
_ <- fat.type.Chunk

# Criando um bloco a partir de texto
x = Chunk('example')

x.size     # 7, o tamanho em bytes
x.toText   # 'example', representado como texto
x.toBytes  # [ 101, 120, 97, 109, 112, 108, 101 ], os valores UTF-8

x.seek(Chunk('am'))         # 2, a posição da correspondência
x.patch(1, 5, Chunk('XY'))  # um novo bloco 'eXYe'

# Criando um bloco a partir de um número
y = Chunk(5)  # cria um bloco de 5 bytes inicializados com zero

y.size     # retorna 5
y.toBytes  # retorna [ 0, 0, 0, 0, 0 ]
```

## Veja também

- [Chunk (sintaxe)](../../syntax/types/chunk.md)
- [Pacote type](index.md)
