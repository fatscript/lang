{% if output.name != "ebook" %}

# HugeInt

{% endif %}

Extensões do protótipo HugeInt

## Importação

```
_ <- fat.type.HugeInt
```

## Construtor

| Nome    | Assinatura | Breve descrição                      |
| ------- | ---------- | ------------------------------------ |
| HugeInt | (val: Any) | Converte número ou texto para HugInt |

> ao converter de `Text`, a entrada é interpretada como uma representação hexadecimal

## Membros do protótipo

| Nome     | Assinatura                            | Breve descrição                         |
| -------- | ------------------------------------- | --------------------------------------- |
| isEmpty  | <> Boolean                            | Retorna verdadeiro se zero              |
| nonEmpty | <> Boolean                            | Retorna verdadeiro se não zero          |
| size     | <> Number                             | Retorna número de bits para representar |
| toText   | <> Text                               | Retorna número em texto hexadecimal     |
| freeze   | <> Void                               | Torna o valor imutável                  |
| modExp   | (exp: HugeInt, mod: HugeInt): HugeInt | Retorna exponenciação modular           |
| toNumber | <> Number                             | Converte para número (com perda)        |
| toChunk  | <> Chunk                              | Codifica para representação binária     |

### Exemplos

```
_ <- fat.type.HugeInt

# Convertendo HugeInt
x = 0x1f4  # 500 em hexadecimal
x.toText   # retorna '1f4'
x.toNumber # retorna 500
x.size     # 9 bits são necessários para representar 500

# Exponenciação modular
y = 0x3          # 3 em hexadecimal
z = 0x5          # 5 em hexadecimal
mod = 0x7        # 7 em hexadecimal
y.modExp(z, mod) # 0x5, equivalente a (3^5) % 7
```

### Notas de uso

#### Conversão de Number para HugeInt

- O valor máximo permitido para conversão de `Number` é `2^53`.
- Tentar passar um valor maior que `2^53` gerará um `ValueError`.

#### Conversão de HugeInt para Number

- Valores até `2^1023 - 1` podem ser convertidos, embora alguma perda de precisão possa ocorrer para valores muito grandes.
- Se o valor exceder esse limite, o resultado será `infinity`. Isso pode ser verificado usando o método `isInf` da [biblioteca matemática](../math.md).

> a biblioteca de matemática também fornece o valor `maxInt`, que serve para avaliar a potencial perda de precisão; se um número é menor que `maxInt`, sua conversão de `HugeInt` é considerada segura sem perda de precisão

## Veja também

- [HugeInt (sintaxe)](../../syntax/types/hugeint.md)
- [Pacote type](index.md)
