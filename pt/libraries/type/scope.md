{% if output.name != "ebook" %}

# Scope

{% endif %}

Extensões do protótipo Scope

## Importação

```
_ <- fat.type.Scope
```

## Alias

Keyset: uma lista de chaves (List/Text)

## Construtor

| Nome  | Assinatura | Breve descrição           |
| ----- | ---------- | ------------------------- |
| Scope | (val: Any) | Envolver val em um escopo |

## Membros do protótipo

| Nome     | Assinatura            | Breve descrição                          |
| -------- | --------------------- | ---------------------------------------- |
| isEmpty  | <> Boolean            | Retorna verdadeiro se o tamanho for zero |
| nonEmpty | <> Boolean            | Retorna verdadeiro para tamanho não zero |
| size     | <> Number             | Retorna o número de entradas             |
| toText   | <> Text               | Retorna o literal de texto 'Scope'       |
| freeze   | <> Void               | Torna o valor imutável                   |
| seal     | <> Void               | Impede o crescimento do escopo           |
| isSealed | <> Boolean            | Verifica se o escopo está selado         |
| copy     | <> Scope              | Retorna cópia profunda do escopo         |
| keys     | <> Keyset             | Retorna lista de chaves do escopo        |
| valuesOf | (t: Type): List       | Obtém valores correspondentes ao tipo t  |
| pick     | (keys: Keyset): Scope | Filtra o escopo pelas chaves             |
| omit     | (keys: Keyset): Scope | Filtra o escopo removendo as chaves      |
| vmap     | (m: Method): Scope    | Mapeia valores (m = v1 -> v2)            |
| maybe    | (key: Text): Option   | Retorna valor dentro de Option           |

### Exemplo

```
_ <- fat.type.Scope
x = { num = 12, prop = 'outra' }
x.size  # retorna 2
```

## Veja também

- [Scope (sintaxe)](../../syntax/types/scope.md)
- [Tipo Option](../extra/option.md)
- [Pacote type](index.md)
