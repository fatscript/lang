{% if output.name != "ebook" %}

# Text

{% endif %}

Extensões do protótipo Text

## Importação

```
_ <- fat.type.Text
```

## Construtor

| Nome | Assinatura | Breve descrição                         |
| ---- | ---------- | --------------------------------------- |
| Text | (val: Any) | Coage valor para texto, igual a .toText |

## Membros do protótipo

| Nome       | Assinatura                      | Breve descrição                                 |
| ---------- | ------------------------------- | ----------------------------------------------- |
| isEmpty    | <> Boolean                      | Retorna verdadeiro se o comprimento for zero    |
| nonEmpty   | <> Boolean                      | Retorna verdadeiro se comprimento não-zero      |
| size       | <> Number                       | Comprimento do texto de retorno                 |
| toText     | <> Text                         | Retorna o próprio valor                         |
| freeze     | <> Void                         | Torna o valor imutável                          |
| replace    | (old: Text, new: Text): Text    | Substituir old por new (todos)                  |
| indexOf    | (frag: Text): Number            | Obter índice de fragmento, -1 se ausente        |
| contains   | (frag: Text): Boolean           | Verifica se o texto contém fragmento            |
| count      | (frag: Text): Number            | Obtém contagem de repetições do fragmento       |
| startsWith | (frag: Text): Boolean           | Verifica se começa com o fragmento              |
| endsWith   | (frag: Text): Boolean           | Check if ends with fragment                     |
| split      | (sep: Text): List/Text          | Divide texto por sep em lista                   |
| toLower    | <> Text                         | Retorna a versão minúscula do texto             |
| toUpper    | <> Text                         | Retorna a versão maiúscula do texto             |
| trim       | <> Text                         | Retorna a versão aparada do texto               |
| isBlank    | <> Boolean                      | Retorna verdadeiro se apenas espaços em branco  |
| match      | (re: Text): Boolean             | Retorna se o texto corresponde à regex          |
| groups     | (re: Text): Scope               | Retorna grupos correspondentes à regex          |
| repeat     | (n: Number): Text               | Retorna o texto repetido n vezes                |
| overlay    | (base: Text, align: Text): Text | Retorna o texto sobreposto a base               |
| patch      | (i, n, val: Text): Text         | Insere val na posição i, removendo n caracteres |
| toChunk    | <> Chunk                        | Codifica para representação binária             |

### Exemplo

```
_ <- fat.type.Text
x = 'banana'
x.size                        # retorna 6
x.replace('nana', 'nquete');  # produz 'banquete'
```

### regex

Ao definir expressões regulares, prefira usar [textos brutos](../../syntax/types/text.md#textos-brutos) e lembre-se de escapar as barras invertidas conforme necessário, garantindo que as expressões regulares sejam interpretadas corretamente:

```
alphaOnly = "^[[:alpha:]]+$"
'abc'.match(alphaOnly) == true

ipAddress = "^([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})$"
'192.168.1.2'.groups(ipAddress) == {
  _0 = '192.168.1.2'
  _1 = '192'
  _2 = '168'
  _3 = '1'
  _4 = '2'
}
```

> o dialeto implementado é [POSIX regex estendido](https://en.wikibooks.org/wiki/Regular_Expressions/POSIX-Extended_Regular_Expressions)

### overlay (sobreposição)

O valor padrão de alinhamento (se não fornecido) é 'left' (esquerda). Outros valores possíveis são 'center' (centro) e 'right' (direita):

```
'x'.overlay('___')            # 'x__'
'x'.overlay('___', 'left')    # 'x__'
'x'.overlay('___', 'center')  # '_x_'
'x'.overlay('___', 'right')   # '__x'
```

> o resultado é sempre do mesmo tamanho que o parâmetro `base`, o texto será cortado se for mais longo

## Veja também

- [Text (sintaxe)](../../syntax/types/text.md)
- [Pacote type](index.md)
