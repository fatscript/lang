{% if output.name != "ebook" %}

# recode

{% endif %}

Conversão de dados entre vários formatos

## Importação

```
_ <- fat.recode
```

> o [pacote type](type/index.md) é automaticamente importado com esta importação

## Variáveis

Estas configurações podem ser usadas para ajustar o comportamento das funções de processamento:

- csvSeparator, o padrão é `,` (vírgula)
- csvQuote, o padrão é `"` (aspas duplas)

## Funções Base64

| Nome       | Assinatura          | Breve descrição                               |
| ---------- | ------------------- | --------------------------------------------- |
| toBase64   | (data: Chunk): Text | Codifica bloco binário para texto base64      |
| fromBase64 | (b64: Text): Chunk  | Decodifica texto base64 para formato original |

## Funções JSON

| Nome     | Assinatura        | Breve descrição                         |
| -------- | ----------------- | --------------------------------------- |
| toJSON   | (val: Any): Text  | Codifica JSON a partir de tipos nativos |
| fromJSON | (json: Text): Any | Decodifica JSON para tipos nativos      |

> com `toJSON` os tipos nativos como `HugeInt`, `Method` e `Chunk` serão convertidos em `null`, enquanto `Errors` serão convertidos para texto

## Funções URL

| Nome         | Assinatura          | Breve descrição                                     |
| ------------ | ------------------- | --------------------------------------------------- |
| toURL        | (text: Text): Text  | Codifica texto para texto escapado URL              |
| fromURL      | (url: Text): Text   | Decodifica texto escapado URL para formato original |
| toFormData   | (data: Scope): Text | Codifica escopo para Dados de Formulário URL        |
| fromFormData | (data: Text): Scope | Decodifica Dados de Formulário URL para escopo      |

## Funções CSV

| Nome    | Assinatura                                  | Breve descrição                 |
| ------- | ------------------------------------------- | ------------------------------- |
| toCSV   | (header: List/Text, rows: List/Scope): Text | Codifica CSV a partir de linhas |
| fromCSV | (csv: Text): List/Scope                     | Decodifica CSV para linhas      |

> a partir da versão `4.x.x` os métodos CSV oferecem suporte a aspas automáticas, aspas de escape, separadores e novas linhas dentro de aspas

## Funções RLE

| Nome    | Assinatura            | Breve descrição            |
| ------- | --------------------- | -------------------------- |
| toRLE   | (chunk: Chunk): Chunk | Comprime para esquema RLE  |
| fromRLE | (chunk: Chunk): Chunk | Descomprime de esquema RLE |

## Cópias congeladas e aquecidos

| Nome        | Assinatura       | Breve descrição                 |
| ----------- | ---------------- | ------------------------------- |
| toFrostCopy | (item: Any): Any | Cria uma cópia imutável do item |
| toHotCopy   | (item: Any): Any | Cria uma cópia mutável do item  |

> `toFrostCopy` garante imutabilidade, sendo ideal para capturar instantâneos de dados aninhados "seguros", enquanto `toHotCopy` permite que os dados sejam aquecidos novamente, útil em cenários onde dados congelados precisam de processamento adicional

## Outras funções

| Nome      | Assinatura        | Breve descrição                       |
| --------- | ----------------- | ------------------------------------- |
| inferType | (val: Text): Any  | Converte texto em void/boolean/number |
| minify    | (src: Text): Text | Minifica código fonte FatScript       |

> `minify` substituirá quaisquer instruções `$break` (ponto de interrupção do depurador) por `()`

Para desativar a inferência de tipo fornecida por `inferType` para `fromFormData` e `fromCSV`, você pode sobrescrevê-la globalmente usando `recode.inferType = -> _` após importar `fat.recode`, ou para reativar `recode.inferType = val -> $inferType`.

## Veja também

- [Pacote type](type/index.md)
- [Biblioteca SDK](sdk.md)
