{% if output.name != "ebook" %}

# enigma

{% endif %}

Métodos de criptografia, hash e UUID

## Importação

```
_ <- fat.enigma
```

## Métodos padrão

Estes métodos estão disponíveis em todas as construções `fry`. Embora `derive`, `encrypt` e `decrypt` ajudem a criar cifras "não legíveis por humanos", eles não são considerados seguros criptograficamente. **NÃO os use sozinhos para proteger dados sensíveis!**

| Nome    | Assinatura                                     | Resumo                               |
| ------- | ---------------------------------------------- | ------------------------------------ |
| getHash | (msg: Text): Number                            | Calcula hash de 32 bits do texto     |
| genUUID | <> Text                                        | Gera um UUID (versão 4)              |
| genKey  | (len: Number): Text                            | Gera uma chave aleatória             |
| derive  | (secret: Text, salt: Text, iter: Number): Text | Função básica de derivação de chave  |
| encrypt | (msg: Text, key: Text = ø): Text               | Criptografa mensagem usando chave    |
| decrypt | (msg: Text, key: Text = ø): Text               | Descriptografa mensagem usando chave |

## Métodos OpenSSL

Estes métodos são seguros criptograficamente e estão disponíveis apenas em builds que incluem suporte ao `OpenSSL`. Eles fornecem ferramentas robustas para lidar com criptografia, descriptografia, hash e derivação de chave com altos padrões de segurança.

| Nome       | Assinatura                                                    | Resumo                            |
| ---------- | ------------------------------------------------------------- | --------------------------------- |
| digest     | (data: Chunk, algo: Text = 'sha256'): HugeInt                 | Calcula um hash seguro            |
| bytes      | (len: Number): Chunk                                          | Gera bytes aleatórios             |
| pbkdf2     | (secret: Text, salt: Text, iter: Number, algo: Text): HugeInt | Deriva uma chave usando PBKDF2    |
| hmac       | (data: Chunk, key: Chunk, algo: Text): HugeInt                | Calcula um código de autenticação |
| encryptAES | (data: Chunk, key: Chunk): Chunk                              | Criptografa com chave AES-256     |
| decryptAES | (data: Chunk, key: Chunk): Chunk                              | Descriptografa dados em AES-256   |

## Notas de uso

### getHash

Um hash de 32 bits é suficiente para proteger contra corrupção de dados em ate 100kb. Fry usa `FNV1A_Jesteress`, que é um dos algoritmos de hash mais rápidos e "suficientemente bons" para strings "longas".

### genUUID

Um UUID, ou Identificador Único Universal, é um número de 128 bits usado para identificar objetos ou entidades em sistemas computacionais. A implementação gera UUIDs aleatórios seguindo o formato da especificação RFC 4122 versão 4, mas não adere estritamente aos padrões de aleatoriedade segura criptograficamente.

### genKey

Gera uma chave aleatória usando o alfabeto Base64.

### derive (inseguro)

Esta função determinística de derivação de chave produz uma string de 32 caracteres usando o alfabeto Base64. Embora possa parecer oferecer 192 bits de entropia, a entropia efetiva **é significativamente menor** devido à função de hash subjacente.

> Nota: A intenção original desta função é também o alongamento de chave. Ela é projetada para ser usada em combinação com as funções `encrypt` e `decrypt`, oferecendo uma camada adicional de processamento para as chaves utilizadas nestas operações de criptografia. Porém, como o método não fornece segurança criptográfica robusta por si só, é recomendado o uso de métodos de derivação de chave mais seguros para aplicações que exijam altos padrões de proteção de dados.

### encrypt/decrypt (inseguro)

Estas funções podem ser usadas com ou sem uma chave especificada (em branco para chave padrão). Elas empregam uma operação simples de XOR combinada com um hash de controle e são codificadas em Base64. Estes métodos **não são criptograficamente seguros** e não devem ser usados para proteger dados sensíveis, a menos que sejam combinados com uma [cifra de uso único](https://pt.wikipedia.org/wiki/One-time_pad).

## Detalhes adicionais para métodos OpenSSL

### digest

Suporta múltiplos algoritmos de hash, incluindo sha1, sha224, sha256, sha384, sha512, sha3_224, sha3_256, sha3_384, sha3_512, permitindo flexibilidade dependendo dos requisitos de segurança.

### bytes

Ideal para criar chaves criptográficas de alta entropia e vetores de inicialização (IVs).

### pbkdf2

Utiliza uma senha, sal e um número especificado de iterações junto com uma função de hash criptográfico para produzir uma chave de criptografia robusta.

### hmac

Garante a integridade dos dados e autenticidade usando uma chave secreta e o algoritmo de hash especificado.

### encryptAES/decryptAES

Operam usando o modo AES-256-CCM, proporcionando confidencialidade e autenticidade. A chave deve ter exatamente 32 bytes, obtida de uma fonte segura como a função pbkdf2, isso normalmente pode ser alcançado assim:

```
key: HugeInt = enigma.pbkdf2(...)
encryptionKey = key.toChunk.fit(32)
```
