{% if output.name != "ebook" %}

# smtp

{% endif %}

Framework de manipulação SMTP

> esta biblioteca fornece uma interface simples para configurar parâmetros SMTP e enviar e-mails

## Importação

```
_ <- fat.smtp
```

## Tipos

A biblioteca `smtp` introduz o tipo `ContactInfo`.

### ContactInfo

O tipo `ContactInfo` representa um contato de e-mail, que pode incluir um nome opcional junto com o endereço de e-mail.

#### Construtor

| Nome        | Assinatura                     | Breve descrição                |
| ----------- | ------------------------------ | ------------------------------ |
| ContactInfo | (email: Text, name: Text = '') | Constrói um objeto ContactInfo |

- **email**: O endereço de e-mail do contato.
- **name** (opcional): O nome do contato.

## Métodos

### config

Configura as definições de SMTP.

| Parâmetro | Tipo        | Descrição                                 |
| --------- | ----------- | ----------------------------------------- |
| from      | ContactInfo | Um objeto que representa o remetente.     |
| server    | Text        | A URL/endereço do servidor SMTP.          |
| username  | Text        | O nome de usuário para autenticação SMTP. |
| password  | Text        | A senha para autenticação SMTP.           |
| useSSL    | Boolean     | Usar SSL/TLS (padrão é true).             |

> gera um erro se a configuração falhar

### send

Envia um e-mail.

| Parâmetro | Tipo             | Descrição                   |
| --------- | ---------------- | --------------------------- |
| to        | List/ContactInfo | Uma lista de destinatários. |
| subject   | Text             | O assunto do e-mail.        |
| body      | Text             | O corpo do e-mail.          |

> retorna o UUID da mensagem em caso de sucesso

## Notas de uso

Exemplo:

```
smtp <- fat.smtp

smtp.config(
  from = ContactInfo('sender@example.com', 'Nome do Remetente')
  server = 'smtps://smtp.exemplo.com:porta'
  username = 'seu_usuario'
  password = 'sua_senha'
)

smtp.send(
  to = [
    ContactInfo('recipient1@example.com', 'Destinatário Um')
    ContactInfo('recipient2@example.com')  # nome é opcional
  ]
  subject = 'E-mail de Teste'
  body = 'Este é um e-mail de teste enviado usando fat.smtp.'
)
```

SSL/TLS é ativado por padrão na configuração SMTP. Se o seu servidor SMTP exigir SSL/TLS, nenhuma configuração adicional é necessária. No entanto, se o seu servidor não suportar SSL/TLS, você pode desativá-lo definindo `useSSL` como `false` ao chamar `config`.

### SMTP na Web Build

Ao usar `fry` compilado com Emscripten (por exemplo, ao usar [FatScript Playground](https://fatscript.org/playground)), não há suporte para esta biblioteca.
