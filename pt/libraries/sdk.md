{% if output.name != "ebook" %}

# sdk

{% endif %}

Utilitários do kit de desenvolvimento de software do fry

> uma biblioteca especial que expõe alguns dos elementos internos do interpretador fry

## Importação

```
_ <- fat.sdk
```

## Métodos

| Nome       | Assinatura            | Breve descrição                                   |
| ---------- | --------------------- | ------------------------------------------------- |
| ast        | (\_): Void            | Imprime árvore de sintaxe abstrata do nó          |
| stringify  | (\_): Text            | Serializa o nó em texto semelhante a JSON         |
| eval       | (\_): Any             | Interpreta texto como programa FatScript          |
| getVersion | <> Text               | Retorna versão do fry                             |
| printStack | (depth: Number): Void | Imprime pilha do contexto de execução             |
| readLib    | (ref: Text): Text     | Retorna o código-fonte da biblioteca fry          |
| typeOf     | (\_): Text            | Retorna o nome do tipo do nó                      |
| getTypes   | <> List               | Retorna info sobre tipos declarados               |
| getDef     | (name: Text): Any     | Retorna definição de tipo por nome                |
| getMeta    | <> Scope              | Retorna os metadados do interpretador             |
| setKey     | (key: Text): Void     | Definir chave para pacotes ofuscados              |
| setMem     | (n: Number): Void     | Definir limite de memória (contagem de nós)       |
| runGC      | <> Number             | Rodar o GC, retorna transcorrido em milissegundos |
| quickGC    | <> Number             | Roda um único ciclo do GC e retorna ms            |

## Notas de uso

### stringify

Enquanto `recode.toJSON` produz um JSON estritamente válido, `stringify` é mais flexível. Ele é capaz de exportar `HugeInt` como números hexadecimais (por exemplo, `0x123abc`), `Chunk` como codificado em Base64, e outros tipos também podem ter representações mais informativas do que apenas `null`. Essas representações são projetadas para permitir uma exportação mais rica para o ambiente FatScript e não são destinadas à serialização compatível com JSON.

### readLib

```
_ <- fat.sdk
_ <- fat.console

print(readLib('fat.extra.Date'))  # imprime a implementação da biblioteca Date
```

> `readLib` não pode ver arquivos externos, mas `read` da [biblioteca file](file.md) pode

### setKey

Use preferencialmente no arquivo `.fryrc` assim:

```
_ <- fat.sdk
setKey('secret')  # irá codificar e decodificar pacotes com esta chave
```

Veja mais sobre [ofuscação](../general/bundling.md#ofuscação).

### setMem

Use preferencialmente no arquivo `.fryrc` assim:

```
_ <- fat.sdk
setMem(5000)  # ~2mb
```

### Escolhendo entre GC completo e rápido

A maioria dos scripts simples em FatScript não precisará se preocupar com a gestão de memória, pois as configurações padrão são projetadas para fornecer uma capacidade de memória ampla e um comportamento automático eficiente desde o início. Geralmente, a melhor maneira de otimizar a performance é simplesmente ajustando o limite de memória. Em alguns casos raros, como em um loop de jogo ou processos iterativos complexos, você pode se beneficiar de chamar explicitamente o GC.

O método `quickGC` realiza uma limpeza rápida e menos exaustiva, tornando-o adequado para cenários onde é aceitável uma certa flexibilidade na alocação de memória. Por outro lado, `runGC` garante uma coleta de lixo mais completa, mas pode resultar em tempos de execução mais longos, dependendo de fatores como o tamanho e a complexidade do grafo de memória. No entanto, `quickGC` pode levar ao acúmulo de memória não reclamada, tornando-o menos eficaz em certos contextos. A melhor maneira de determinar a opção mais adequada é realizar testes comparativos em sua aplicação, simulando cenários de uso real.

> rode seu script com a flag `-c` para avaliar o desempenho de sua execução

Veja mais sobre [gerenciamento de memória](../general/options.md#gerenciamento-de-memória).

## Veja também

- [Biblioteca recode](recode.md)
