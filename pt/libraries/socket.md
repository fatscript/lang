{% if output.name != "ebook" %}

# socket

{% endif %}

Manipulação de sockets TCP

> oferece suporte opcional a SSL/TLS (caso a biblioteca OpenSSL esteja disponível)

## Importação

```
_ <- fat.socket
```

## Estruturas

### ClientSocket

Representa uma conexão de cliente, ou seja, um socket conectado a um servidor remoto.

#### Propriedades

| Nome | Tipo   | Breve descrição                        |
| ---- | ------ | -------------------------------------- |
| id   | Number | Descritor de arquivo interno do socket |
| peer | Text   | Metadados sobre o endpoint remoto      |
| ssl  | Chunk  | Usado internamente para sessões SSL    |

#### Métodos

| Nome    | Assinatura              | Breve descrição          |
| ------- | ----------------------- | ------------------------ |
| receive | (nBytes: Number): Chunk | Lê até nBytes do socket  |
| send    | (data: Chunk): Void     | Envia data para o socket |
| close   | <> Void                 | Fecha a conexão          |

> `receive(nBytes)` irá bloquear até obter algum dado ou a conexão seja fechada

### ServerSocket

Representa um socket em modo servidor, que aguarda conexões de clientes.

#### Propriedades

| Nome | Tipo   | Breve descrição                        |
| ---- | ------ | -------------------------------------- |
| id   | Number | Descritor de arquivo interno do socket |

#### Métodos

| Nome   | Assinatura                   | Breve descrição                          |
| ------ | ---------------------------- | ---------------------------------------- |
| accept | (wait: Number): ClientSocket | Aguarda por uma conexão por até wait ms. |
| close  | <> Void                      | Fecha o socket servidor                  |

## Métodos avulsos

| Nome      | Assinatura                                 | Breve descrição                  |
| --------- | ------------------------------------------ | -------------------------------- |
| connect   | (addr, port, useSSL = false): ClientSocket | Conecta-se a um servidor remoto  |
| verifySSL | (enabled: Boolean): Void                   | Configuração SSL (modo cliente)  |
| setSSL    | (certPath: Text, keyPath: Text): Void      | Configuração SSL (mode servidor) |
| bind      | (port: Number): ServerSocket               | Inicia servidor na porta         |

## Notas de eso

### Modo cliente

#### Estabelecendo conexão

Use a função `connect(addr, port, useSSL)` para criar um `ClientSocket`. Por exemplo:

```
_      <- fat.type._
socket <- fat.socket

con = socket.connect("example.org", 80)
con.send("GET / HTTP/1.1\r\nHost: example.org\r\n\r\n".toChunk)
response = con.receive(1024)
...
con.close
```

#### Verificação de certificados

- Para habilitar SSL/TLS, passe `true` para o parâmetro `useSSL` em `connect`.
- Por padrão a verificação de certificados está ativa quando SSL está sendo utilizado. Caso utilize certificados autoassinados em seu servidor de testes, você pode chamar `verifySSL(false)` para desabilitar a verificação:

```
socket.verifySSL(false)
con = socket.connect("localhost", 443, true)
...
```

#### Leitura e escrita

Você pode usar `send` para enviar dados e `receive` para ler dados. Operações de `send` delegam o envio para o sistema operacional e retornam imediatamente, enquanto operações de `receive` bloqueiam até que alguma mensagem seja recebida ou a conexão seja encerrada pela outra parte.

### Modo servidor

#### Escutando em uma porta

Para iniciar um socket em modo servidor, use `bind(port)`, obtendo um objeto `ServerSocket`:

```
socket <- fat.socket

server = socket.bind(1234)
...
```

#### Aceitando conexões

- Chame `server.accept(wait)` para aceitar conexões de clientes. Caso nenhuma conexão surja dentro de `wait` milissegundos, retorna `null`.
- Ao obter um `ClientSocket` a partir de `accept`, você pode interagir com o cliente usando `receive`, `send` e `close`.

```
~ con = server.accept(5000)  # aguarda até 5 segundos
con != null ? {
  msg = con.receive(256)
  ...
  con.send(Chunk("Olá, cliente!"))
  con.close
}
```

> use `wait` de `-1` para bloquear até que uma nova conexão comece, use `wait` de `0` para não bloquear

#### SSL no servidor

Antes de chamar `bind`, use `setSSL("cert.pem", "key.pem")` para carregar um certificado e chave privada e habilitar SSL/TLS:

```
socket.setSSL("cert.pem", "key.pem")
server = socket.bind(443)
```

#### Fechando o servidor

Basta chamar `server.close` quando quiser parar de aceitar novas conexões.

## Veja também

- [Biblioteca async](async.md)
- [Biblioteca HTTP](http.md)
