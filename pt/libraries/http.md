{% if output.name != "ebook" %}

# http

{% endif %}

Framework de manipulação HTTP

## Importação

```
_ <- fat.http
```

### Route

Uma rota é uma estrutura usada para mapear métodos HTTP para certos padrões de caminho, especificando qual código deve ser executado quando uma requisição é recebida. Cada rota pode definir um comportamento diferente para cada método HTTP (`POST`, `GET`, `PUT`, `DELETE`).

#### Construtor

| Nome  | Assinatura                                                           | Breve descrição          |
| ----- | -------------------------------------------------------------------- | ------------------------ |
| Route | (path: Text, post: Method, get: Method, put: Method, delete: Method) | Constrói um objeto Route |

> cada método implementado recebe um HttpRequest como argumento e deve retornar um objeto HttpResponse

### HttpRequest

Um `HttpRequest` representa uma mensagem de requisição HTTP. Isso é o que seu servidor recebe de um cliente quando ele faz uma requisição ao seu servidor.

#### Construtor

| Nome        | Assinatura                                                          | Breve descrição                |
| ----------- | ------------------------------------------------------------------- | ------------------------------ |
| HttpRequest | (method: Text, path: Text, headers: List, params: Scope, body: Any) | Constrói um objeto HttpRequest |

> os items `params` e `body` podem ser omitidos dependendo da requisição recebida

### HttpResponse

Um `HttpResponse` representa uma mensagem de resposta HTTP. Isso é o que um servidor envia de volta ao cliente em resposta a uma requisição HTTP.

> para respostas em modo cliente, `body` será provido como `Text` se um tipo MIME textual for encontrado nos `headers`, caso contrário, será fornecido como `Chunk`

#### Construtor

| Nome         | Assinatura                                      | Breve descrição                 |
| ------------ | ----------------------------------------------- | ------------------------------- |
| HttpResponse | (status: Number, headers: List/Text, body: Any) | Constrói um objeto HttpResponse |

## Métodos

| Nome       | Assinatura                                | Breve descrição                      |
| ---------- | ----------------------------------------- | ------------------------------------ |
| setHeaders | (headers: List): Void                     | Definir cabeçalhos de solicitações   |
| post       | (url: Text, body, wait): HttpResponse     | Criar/postar body para url           |
| get        | (url: Text, wait): HttpResponse           | Ler/obter de url                     |
| put        | (url: Text, body, wait): HttpResponse     | Atualizar/colocar body na url        |
| delete     | (url: Text, wait): HttpResponse           | Excluir em url                       |
| setName    | (name: Text): Void                        | Definir agente/nome do servidor      |
| verifySSL  | (enabled: Boolean): Void                  | Configuração SSL (modo cliente)      |
| setSSL     | (certPath: Text, keyPath: Text): Void     | Configuração SSL (modo servidor)     |
| listen     | (port: Number, routes: List/Route, maxMs) | Provedor de endpoint (modo servidor) |

> `body: Any` e `wait: Number` são sempre parâmetros opcionais, sendo que caso `body` não se enquadre como `Text` ou `Chunk` será automaticamente convertido para JSON no processo de envio e `wait` é o tempo máximo de espera e o padrão é 30.000ms (30 segundos)

> `verifySSL` está habilitado por padrão para o modo cliente

> `setSSL` pode não estar disponível, caso o sistema não tenha a biblioteca OpenSSL

## Notas de uso

### Modo cliente

Em `HttpResponse.body`, você pode precisar decodificar explicitamente uma resposta JSON para `Scope` usando o método `fromJSON`. Para postar um tipo nativo como JSON, você pode codificá-lo usando o método `toJSON`; no entanto, isso não é estritamente necessário, pois será feito implicitamente. Ambos os métodos estão disponíveis na biblioteca [fat.recode](recode.md).

Se os cabeçalhos não forem definidos, o cabeçalho `Content-Type` padrão para `Chunk` será `application/octet-stream`, para `Text` será `text/plain; charset=UTF-8` e para outros tipos, será `application/json; charset=UTF-8` (devido à conversão implícita).

Você pode definir cabeçalhos de solicitação personalizados da seguinte forma:

```
http <- fat.http

url = ...
token = ...
body = ...

http.setHeaders([
  "Accept: application/json; charset=UTF-8"
  "Content-Type: application/json; charset=UTF-8"
  "Authorization: Bearer " + token  # cabeçalhos personalizado
])

http.post(url, body)
```

> definir cabeçalhos substituirá completamente a lista anterior pela nova lista

Ao realizar solicitações assíncronas, você pode precisar chamar `setHeaders`, `setName` e configurar `verifySSL` dentro de cada Worker, já que essas configurações são locais para cada thread.

### Modo servidor

### Lidando com respostas HTTP

Você pode definir o parâmetro opcional `maxMs` ao chamar `listen` para restringir quanto tempo o servidor vai esperar para que cada solicitação transfira seu conteúdo (conexão de entrada), retornando status 408 caso excedido.

O servidor FatScript lida automaticamente com os códigos de status HTTP comuns, como 200, 400, 404, 405, 408, 500 e 501. Sendo 200 o padrão ao construir um objeto `HttpResponse`.

Além dos códigos de status retornados de forma automática, você também pode retornar explicitamente outros códigos de status, como 201, 202, 203, 204, 205, 301, 401 e 403, especificando o código de status no objeto `HttpResponse`, por exemplo: `HttpResponse(status = 401)`. Para todos os códigos mencionadas aqui, o servidor fornece corpos de resposta padrão em texto simples. No entanto, você tem a opção de substituir esses valores padrão e fornecer seus próprios corpos de resposta personalizados, quando necessário.

Ao lidar automaticamente com esses códigos de status e fornecer corpos de resposta padrão, o servidor FatScript simplifica o processo de desenvolvimento, ao mesmo tempo em que permite que você tenha controle sobre o conteúdo da resposta quando necessário.

> não pertencendo a nenhum dos códigos anteriores, o servidor irá retornar o código 500

Veja um exemplo de um simples servidor HTTP de arquivos:

```
_ <- fat.std

# adapte para o local do conteúdo
directory = '/home/user/contentFolder'

# restrito a algumas extensões somente
mediaTypesByExtension = {
  htm = html = 'text/html'
  js = 'application/javascript'
  json = 'application/json'
  css = 'text/css'
  md = 'text/markdown'
  xml = 'application/xml'
  csv = 'text/csv'
  txt = 'text/plain'
  svg = 'image/svg+xml'
  rss = 'application/rss+xml'
  atom = 'application/atom+xml'
  png = 'image/png'
  jpg = jpeg = 'image/jpeg'
  gif = 'image/gif'
  ico = 'image/x-icon'
  webp = 'image/webp'
  woff = 'font/woff'
  woff2 = 'font/woff2'
}

routes: List/Route = [
  Route(
    '*'
    get = (request: HttpRequest): HttpResponse -> {
      path = file.resolve(directory + request.path)  # sanitized path
      type = path & path.startsWith(directory)       # jailed access
        ? mediaTypesByExtension(path.split('.')(-1))

      path.isEmpty => HttpResponse(status = 404)  # not found
      type.isEmpty => HttpResponse(status = 403)  # forbidden
      _            => HttpResponse(
        status = 200
        headers = [ 'Content-Type: {type}' ]
        body = file.readBin(path)
      )
    }
  )
]

http.listen(8080, routes)
```

> use `http.listen(0, routes)` para iniciar um servidor com um número de porta atribuído automaticamente e verifique a porta efetiva atribuída ao servidor através do comando embutido `$port`
