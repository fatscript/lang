{% if output.name != "ebook" %}

# pacote extra

{% endif %}

Tipos adicionais implementados em FatScript puro:

- [Date](date.md) - Gerenciamento de calendário e datas
- [Duration](duration.md) - Construtor de duração em milissegundos
- [Fuzzy](fuzzy.md) - Valores probabilísticos e operações de lógica fuzzy
- [HashMap](hmap.md) - Armazenamento rápido de chave-valor
- [Logger](logger.md) - Suporte ao registro de logs
- [Memo](memo.md) - Utilitário de memoização genérica
- [MouseEvent](mouse.md) - Parser de eventos de mouse
- [Opaque](opqaue.md) - Utilitário para proteção suave de dados
- [Option](option.md) - Encapsulamento de valor opcional
- [Param](param.md) - Verificação de presença e tipo de parâmetro
- [Sound](sound.md) - Interface de reprodução de som
- [Storable](storable.md) - Armazenamento de dados

## Importando

Se você quiser disponibilizar todos eles de uma só vez, basta escrever:

```
_ <- fat.extra._
```

...ou importe um por um, conforme necessário, por exemplo:

```
_ <- fat.extra.Date
```
