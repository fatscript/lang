{% if output.name != "ebook" %}

# Opaque

{% endif %}

Utilitário para proteção suave de dados

## Importação

```
_ <- fat.extra.Opaque
```

> o [tipo Scope](../type/scope.md) é automaticamente importado junto com esta importação.

## Protótipo

Esta biblioteca introduz o tipo `Opaque`, projetado como um utilitário para encapsular dados com proteção suave. Ele fornece um invólucro em torno de um `Scope`, impedindo o acesso direto à estrutura subjacente quando serializando, mas permitindo interações com seus membros.

O tipo `Opaque` estende o tipo `Scope`, herdando todos os seus membros sem introduzir recursos adicionais.

## Exemplo de uso

```
visible = { ~ a = 1, b = 2 }

# Criando um invólucro opaco
opaque = Opaque(visible)

# Modificando valores no escopo oculto
opaque.c = 3  # funciona como um Scope comum

# O escopo oculto é excluído da serialização no nível superior
recode.toJSON({ opaque, visible })  # '{"opaque":null,"visible":{"a":1,"b":2}}'

# A serialização direta do escopo oculto ainda funciona
recode.toJSON(opaque)  # '{"a":1,"b":2,"c":3}'
```

### Notas de desempenho

O tipo `Opaque` introduz uma camada de indireção que pode impactar o desempenho em cenários que envolvem acesso ou modificações frequentes nos dados ocultos. Considere usá-lo de forma seletiva para casos que exigem encapsulamento, por exemplo, ao armazenar credenciais em uma instância.

### Veja também

- [Tipo Scope](../type/scope.md)
- [Biblioteca Recode](../recode.md)
