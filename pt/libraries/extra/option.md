{% if output.name != "ebook" %}

# Option

{% endif %}

Encapsulamento de valor opcional

## Importação

```
_ <- fat.extra.Option
```

> o [tipo Error](../type/error.md) é automaticamente importado junto com esta importação

## Tipos

Esta biblioteca introduz dois principais tipos: `Some` e `None`, que são casos especiais do tipo `Option`, fornecendo uma maneira de representar valores opcionais, encapsulando a presença (`Some`) ou ausência (`None`) de um valor.

## Membros do protótipo

| Nome      | Assinatura                  | Breve descrição                      |
| --------- | --------------------------- | ------------------------------------ |
| isEmpty   | <> Boolean                  | Verifica se a opção é None           |
| nonEmpty  | <> Boolean                  | Verifica se a opção é Some           |
| size      | <> Number                   | Retorna 1 se Some, 0 se None         |
| toText    | <> Text                     | Retorna o literal de texto           |
| freeze    | <> Void                     | Torna o valor imutável               |
| get       | <> Any                      | Retorna valor ou erro NoSuchElement  |
| getOrElse | (default: Any): Any         | Retorna valor ou default se for None |
| map       | (fn: Method): Option        | Aplica o método ao valor contido     |
| flatMap   | (fn: Method/Option): Option | Aplica método que retorna Option     |
| filter    | (predicate: Method): Option | Filtra o valor pelo predicado        |
| toList    | <> List                     | Converte a opção para Lista          |
| concrete  | <> Option                   | Resolve a opção para Some ou None    |

## Exemplo de uso

```
_ <- fat.extra.Option

# Criando opções
x = Some(5)  # equivalente a Option(5).concrete
y = None()   # equivalente a Option().concrete

# Trabalhando com opções
isEmptyX = x.isEmpty   # false
isEmptyY = y.isEmpty   # true
valX = x.getOrElse(0)  # 5
valY = y.getOrElse(0)  # 0

# Aplicando uma transformação
transformedX = x.map(v -> v * 2).getOrElse(0)  # 10
transformedY = y.map(v -> v * 2).getOrElse(0)  # 0

# Elevando valores a opção
label: Text = Option(opVal).concrete >> {
  Some => 'algum valor'   # caso onde opVal não é null
  None => 'nenhum valor'  # caso onde opVal é null
}
```

### Option em Programação Funcional

No FatScript, `null` é integrado como um cidadão de primeira classe, permitindo que, na maioria dos casos, tipos nativos manipulem valores ausentes sem a necessidade de construtos adicionais para segurança. Consequentemente, o tipo `Option` está incluído no pacote `extra` como açúcar sintático.

Ele permite a encapsulação explícita de valores opcionais para clareza semântica ou aderência a certos paradigmas de programação funcional. Um exemplo de sua utilidade é demonstrado no tipo `Scope`, que inclui um método `maybe` além da sintaxe padrão de recuperação de valor:

- `myScope('key')` retorna o valor associado a `key` ou `null` se a chave não existir.
- `myScope.maybe('key')` fornece um valor envolto em `Option`, distinguindo explicitamente entre a existência (`Some`) e a ausência (`None`) de um valor.

### Manipulação semântica de valores ausentes

Um dos principais benefícios de usar o tipo `Option` é sua capacidade de lidar semanticamente e com segurança com operações que envolvem valores potencialmente ausentes. Esse recurso é particularmente útil em operações primitivas ou transformações de dados onde valores `null` poderiam, de outra forma, levar a erros. Por exemplo, considere um cenário onde você precisa somar um número com um valor que pode não estar presente:

```
# Supondo que ovosComprados esteja definido e tenha um valor
ovosComprados: Number = ...

# geladeira.maybe('ovo') recupera o número de ovos na geladeira como uma Option
# Se 'egg' não estiver presente, o padrão é 0, evitando erros relacionados a null
totalDeOvos: Number = geladeira.maybe('ovo').getOrElse(0) + ovosComprados
```

### Considerações de desempenho

O uso de tipos `Option` introduz sobrecarga computacional devido às chamadas de função necessárias para manipular valores e à memória adicional decorrente de sua estrutura subjacente. Embora os benefícios de segurança e expressividade sejam significativos, o custo de desempenho pode se tornar perceptível em loops apertados ou ao processar grandes conjuntos de dados.

## Veja também

- [Tipo Scope](../type/scope.md)
- [Tipo Error](../type/error.md)
