{% if output.name != "ebook" %}

# Duration

{% endif %}

Construtor de duração em milissegundos

> no FatScript, o tempo é nativamente expresso em milissegundos, e esse tipo fornece uma maneira simples de expressar diferentes magnitudes de tempo em `Millis`

## Importação

```
_ <- fat.extra.Duration
```

## Construtor

| Nome     | Assinatura    | Breve descrição                        |
| -------- | ------------- | -------------------------------------- |
| Duration | (val: Number) | Cria um conversor de duração em Millis |

## Membros do protótipo

| Nome    | Assinatura | Breve descrição                       |
| ------- | ---------- | ------------------------------------- |
| nanos   | <> Millis  | Interpretar valor como nanossegundos  |
| micros  | <> Millis  | Interpretar valor como microssegundos |
| millis  | <> Millis  | Interpretar valor como milissegundos  |
| seconds | <> Millis  | Interpretar valor como segundos       |
| minutes | <> Millis  | Interpretar valor como minutos        |
| days    | <> Millis  | Interpretar valor como dias           |
| weeks   | <> Millis  | Interpretar valor como semanas        |

### Exemplo

```
_    <- fat.extra.Duration
time <- fat.time

cincoSegundos = Duration(5).segundos
time.wait(cincoSegundos)  # pausa a execução do thread por 5 segundos
```
