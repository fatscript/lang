{% if output.name != "ebook" %}

# Param

{% endif %}

Verificação de presença e tipo de parâmetro

## Importação

```
_ <- fat.extra.Param
```

> o [tipo Erro](../type/error.md) é automaticamente importado com esta importação

## Tipos

Esta biblioteca introduz o tipo `Param` e a utilidade `Using/UsingStrict` para declaração de parâmetros implícitos.

## Construtores

Tanto `Param` quanto `Using/UsingStrict` recebem dois argumentos:

- **\_exp: Text**: o nome do parâmetro a ser verificado no contexto.
- **\_typ: Type**: o tipo esperado do valor avaliado.

Adicionalmente, `Param` aceita um argumento opcional:

- **strict: Boolean**: desativa a [correspondência flexível](../../syntax/types/index.md#aceitação-flexível-de-tipos) (padrão é falso).

> `Using` tem essa flag como `false` e `UsingStrict` como `true`

### Param

O tipo `Param` oferece mecanismos para verificar a presença e o tipo de parâmetros no contexto de execução.

#### Membros do protótipo

| Nome | Assinatura | Breve descrição                              |
| ---- | ---------- | -------------------------------------------- |
| get  | <> Any     | Recupera o parâmetro se corresponder ao tipo |

> o método `get` gera `KeyError` se o parâmetro não estiver definido, e `TypeError` se o tipo não corresponder

#### Exemplo

```
_ <- fat.extra.Param
_ <- fat.type.Text  # o tipo desejado deve ser carregado

currentUser = Param('userId', Text)

...

# Supondo que userId esteja definido no contexto e seja um texto,
# recupere seu valor de forma segura do namespace atual
userId = currentUser.get
```

### Using

Aplique `Using/UsingStrict` para suprimir dicas de parâmetros implícitos em declarações de métodos para entradas esperadas no escopo.

> alternativamente, para suprimir avisos sobre parâmetros implícitos, nomeie a entrada implícita começando com um sublinhado (`_`)

#### Exemplo

```
_ <- fat.extra.Param
_ <- fat.type.Text

printUserIdFromContext = <> {
  Using('userId', Text)
  console.log(userId)
}
```

> se o parâmetro implícito estiver ausente ou não corresponder, um erro será gerado em tempo de execução quando o método for chamado

## Veja também

- [Pacote extra](index.md)
