{% if output.name != "ebook" %}

# MouseEvent

{% endif %}

Parser de eventos de mouse para `fat.curses.readKey`

## Importação

```
_ <- fat.extra.MouseEvent
```

## Construtor

| Nome       | Assinatura  | Breve descrição                  |
| ---------- | ----------- | -------------------------------- |
| MouseEvent | (val: Text) | Faz o parse de um evento readKey |

O construtor `MouseEvent` recebe o seguinte argumento:

- **val**: O valor de texto retornado por `fat.curses.readKey`, que é interpretado para extrair dados do evento de mouse (como tipo de clique, posição e estado de liberação).

> se `val` não for um evento de mouse válido, `MouseEvent` retornará `null`

## Membros do protótipo

| Nome      | Assinatura | Breve descrição                         |
| --------- | ---------- | --------------------------------------- |
| name      | <> Text    | Retorna nome legível do evento de mouse |
| code      | Text       | O código que representa a ação do mouse |
| x         | Number     | A coordenada X do mouse                 |
| y         | Number     | A coordenada Y do mouse                 |
| isRelease | Boolean    | O evento é uma liberação de botão?      |

### Nomeação dos eventos

O método `name` pode retornar os seguintes valores legíveis, com base no código do evento e nas teclas modificadoras:

- **Ações de Pressionar**: `leftPress`, `middlePress`, `rightPress`
- **Ações de Soltar**: `leftRelease`, `middleRelease`, `rightRelease`
- **Ações de Arrastar**: `leftDrag`, `middleDrag`, `rightDrag`
- **Ações de Rolagem**: `scrollUp`, `scrollDown`
- **Modificadores**: `Shift+`, `Alt+`, `Ctrl+` (prefixados às ações acima)

## Exemplo

`MouseEvent` é útil para converter eventos brutos em dados utilizáveis, como a posição do mouse e a ação realizada:

```
_       <- fat.extra.MouseEvent
console <- fat.console
curses  <- fat.curses

# Ativar o rastreamento do mouse
curses.setMouse(true)

# Capturar eventos do curses
(~ key = curses.readKey) == Void @ {
  # Aguarde até que um evento ocorra... (no-op)
}

# Fazer o parse do evento do curses
(mEvt = MouseEvent(key)) => {
  console.log('Evento de mouse:')
  console.log('  Código: {mEvt.code}')
  console.log('  X: {mEvt.x}')
  console.log('  Y: {mEvt.y}')
  console.log('  Liberado: {mEvt.isRelease}')
  console.log('  Ação: {mEvt.name}')
}
_ => console.log('Evento não relacionado ao mouse: {key}')
```

## Veja também

- [Biblioteca curses](../curses.md)
