{% if output.name != "ebook" %}

# Fuzzy

{% endif %}

Valores probabilísticos e operações de lógica fuzzy

## Importar

```
_ <- fat.extra.Fuzzy
```

## Construtor

| Nome  | Assinatura          | Breve descrição                      |
| ----- | ------------------- | ------------------------------------ |
| Fuzzy | (val: Number = 0.5) | Cria um valor de probabilidade Fuzzy |

> a faixa de 0 a 1 é ideal para valores, porém, valores mais altos ainda podem ser significativos em operações específicas como conjunção com valores dentro da faixa padrão

## Membros do protótipo

| Nome     | Assinatura            | Breve descrição                                     |
| -------- | --------------------- | --------------------------------------------------- |
| isEmpty  | <> Boolean            | Verifica se a probabilidade é zero                  |
| nonEmpty | <> Boolean            | Verifica se a probabilidade é maior que zero        |
| size     | <> Number             | Converte o valor fuzzy para uma escala percentual   |
| toText   | <> Text               | Converte o valor fuzzy para uma porcentagem textual |
| freeze   | <> Void               | Torna o valor imutável                              |
| and      | (other: Fuzzy): Fuzzy | Operação lógica AND com outro valor fuzzy           |
| or       | (other: Fuzzy): Fuzzy | Operação lógica OR com outro valor fuzzy            |
| not      | <> Fuzzy              | Operação lógica NOT, invertendo a chance            |
| decide   | <> Boolean            | Decide um resultado booleano dentro de sua chance   |

## Uso

```
_ <- fat.extra.Fuzzy

# Criando instâncias fuzzy
lowChance = Fuzzy(0.25)  # chance de 25%
highChance = Fuzzy(0.75) # chance de 75%

# Aplicando operações lógicas
combinedChance = lowChance.and(highChance)
resolvedChance = combinedChance.decide  # resulta em um booleano
```

### Inspiração

A introdução do tipo `Fuzzy` no `FatScript` foi inspirada pela definição humorística da linguagem meme, [DreamBerd](https://github.com/TodePond/DreamBerd), que oferece booleanos que podem ser `verdadeiro`, `falso` ou `talvez`. Aqui, a palavra-chave `talvez` é traduzida para `Fuzzy().decide`, o que pode ser considerado uma construção incomum para a maioria das linguagens de programação e é análogo a jogar uma moeda.

Embora o `FatScript` não seja tão esotérico a ponto de armazenar booleanos como "um bit e meio", o conceito de fornecer um tipo "engraçado" que permite modelar incertezas foi um experimento interessante e pode realmente ser útil em muitos cenários. Ele melhora a capacidade da linguagem de lidar com operações envolvendo chances e processos de tomada de decisão onde os resultados não são determinísticos. O tipo `Fuzzy` é útil para cenários que requerem uma abordagem com nuances da lógica booleana, comumente vista na lógica de jogos e em qualquer lugar onde decisões probabilísticas sejam necessárias.

## Veja também

- [Tipo Booleano](../type/boolean.md)
- [Biblioteca de matemática](../library/math.md)
