{% if output.name != "ebook" %}

# Configuração

{% endif %}

Para começar a "fritar" seu código "gordo", você precisará de um interpretador para a linguagem de programação FatScript.

## fry, O Interpretador FatScript

[fry](https://gitlab.com/fatscript/fry) é um interpretador e ambiente de execução gratuito para FatScript. Você pode instalá-lo em sua máquina seguindo as instruções a seguir.

## Instruções

`fry` é projetado para GNU/Linux, mas também pode funcionar em [outros sistemas operacionais](#suporte-de-sistema-operacional).

Para distribuições baseadas em Arch, instale através do pacote AUR [fatscript-fry](https://aur.archlinux.org/packages/fatscript-fry).

Para outras distribuições, experimente o script de instalação automática:

```bash
curl -sSL https://gitlab.com/fatscript/fry/raw/main/get_fry.sh -o get_fry.sh;
bash get_fry.sh || sudo bash get_fry.sh
```

Ou, para instalar `fry` manualmente:

- Clone o repositório:

```bash
git clone --recursive https://gitlab.com/fatscript/fry.git
```

- Depois, execute o script de instalação:

```bash
cd fry
./install.sh
```

> a instalação manual pode copiar o binário `fry` para a pasta $HOME/.local/bin, alternativamente, use `sudo` para instalá-lo em /usr/local/bin/

- Verifique se o `fry` foi instalado, executando:

```bash
fry --version
```

### Dependências

Se a instalação falhar, podem estar faltando algumas dependências. `fry` requer `git`, `gcc` e `libcurl` para compilar. Por exemplo, para instalar essas dependências no Debian/Ubuntu, execute:

```bash
apt update
apt install git gcc libcurl4-openssl-dev
```

#### Back-end para entrada de texto

`linenoise` é uma dependência leve e uma alternativa ao `readline`, mantida como um submódulo. Se foi não foi incluída durante a operação inicial de `git clone`, você pode corrigir isso com os seguintes comandos:

```bash
git submodule init
git submodule update
```

Se você preferir "linkar" com o `readline`, apenas certifique-se de que ele esteja instalado, executando:

```bash
apt install libreadline-dev
```

## Suporte de Sistema Operacional

`fry` é primordialmente projetado para GNU/Linux, mas também é acessível em outros sistemas operacionais:

### Android

Se você estiver no Android, pode instalar o `fry` via [Termux](https://termux.dev/). Basta instalar as dependências necessárias da seguinte maneira:

```bash
pkg install git clang
```

Em seguida, você pode seguir as instruções padrão de instalação do `fry`.

### ChromeOS

Se você estiver usando o ChromeOS, pode habilitar o suporte ao Linux seguindo as instruções [aqui](https://chromeos.dev/en/linux/setup).

### MacOS

Se você estiver usando o MacOS, precisará ter as [Command Line Tools](https://developer.apple.com/forums/thread/670389) instaladas.

### iOS

Se você estiver usando o iOS, poderá usar o `fry` via [iSH](https://github.com/ish-app/ish/). Primeiro, instale as dependências necessárias:

```bash
apk add bash gcc libc-dev curl-dev
```

Em seguida, de acordo com esta [discussão](https://github.com/ish-app/ish/issues/943), configure o `git` para funcionar corretamente, assim:

```bash
wget https://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86/git-2.24.4-r0.apk
apk add ./git-2.24.4-r0.apk
git config --global pack.threads "1"
```

### Windows

Se você estiver usando o Windows, poderá usar o `fry` via [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/pt-br/windows/wsl/install).

## Imagem Docker

`fry` também está disponível como uma [imagem docker](https://hub.docker.com/r/fatscript/fry/tags):

```bash
docker run --rm -it fatscript/fry
```

Para executar um arquivo FatScript com o docker, use o seguinte comando:

```bash
docker run --rm -it -v ~/project:/app fatscript/fry prog.fat
```

> substitua ~/project pelo caminho para o seu arquivo FatScript

## Solução de problemas

Se você encontrar qualquer problema ou bug ao usar o `fry`, por favor [abra uma "issue"](https://gitlab.com/fatscript/fry/issues).
