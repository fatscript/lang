{% if output.name != "ebook" %}

# Visão Geral

{% endif %}

FatScript é uma linguagem de programação leve e interpretada projetada para criar aplicativos baseados em console. Ela enfatiza a simplicidade, facilidade de uso e conceitos de programação funcional.

## Mas, peraí...

> **Fat**Script, uma linguagem de programação **leve**?

Sim, há algo estranho nessa afirmação... mas, por favor, deixe-me explicar.

A expressão "açúcar sintático" refere-se a recursos que facilitam a escrita do código, escondendo a complexidade subjacente. E, assim como consumir muito açúcar... pode haver consequências: gordura (fat, em inglês). O que, nesse sentido, é algo positivo - muito peso em poucas linhas de código.

Dito isso, o FatScript ainda é uma linguagem relativamente nova e, embora tenha sido projetada para ser simples e intuitiva, pode não ser a melhor escolha para todas as tarefas, especialmente quando se trata de computação de alto desempenho ou cargas de trabalho extremamente complexas. No entanto, apesar do seu nome, o interpretador do FatScript é pequeno (leve), com um custo de inicialização quase zero e os [benchmarks](https://www.youtube.com/watch?v=EH7FtBdb3pE) mostram que ele tem um desempenho comparável ao de linguagens como Python ou JavaScript.

Então, embora chamar o FatScript de "leve" possa ser discutível, o runtime da linguagem não é inerentemente pesado e mantém um perfil eficiente na prática para a maioria dos casos de uso.

## Conceitos chave

- Gerenciamento automático de memória por coleta de lixo (GC)
- Combinações simbólicas de caracteres para uma sintaxe minimalista
- REPL (Read-Eval-Print Loop) para testes rápidos de expressões
- Suporte para sistema de tipos, herança e subtipagem por meio de aliases
- Suporte para programação imutável e métodos passáveis ​(como valores)
- Manter se simples e intuitivo, sempre que possível

## Livre e de código aberto

`fatscript/fry` é um projeto de código aberto que incentiva a colaboração e o compartilhamento de conhecimento. Nós convidamos os desenvolvedores a [contribuir](https://gitlab.com/fatscript/fry/blob/main/CONTRIBUTING.md) para o projeto e nos ajudar a melhorá-lo com o tempo.

## Conteúdo desta seção

- [Configuração](setup.md): como instalar o interpretador de FatScript
- [Opções](options.md): como personalizar a execução
- [Empacotamento](bundling.md): como empacotar um aplicativo FatScript
- [Ferramentas](tooling.md): visão geral de algumas ferramentas e recursos extras
