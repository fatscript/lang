{% if output.name != "ebook" %}

# Empacotamento

{% endif %}

O fry oferece uma ferramenta integrada de empacotamento para código FatScript.

## Utilização

Para agrupar seu projeto em um único arquivo a partir do ponto de entrada, execute:

```
fry -b sweet mySweetProject.fat
```

A seguir, você pode executar seu programa:

```
./sweet
```

Este processo faz o seguinte:

- Consolida todas as importações, exceto as bibliotecas padrão e [caminhos literais](../syntax/imports.md#literal-paths)
- Remove espaços e comentários para melhorar os tempos de carregamento
- Substitui quaisquer instruções `$break` (ponto de interrupção do depurador) por `()`
- Adiciona um [shebang](https://bash.cyberciti.biz/guide/Shebang) ao código empacotado
- Recebe o atributo de execução para o modo de arquivo

## Considerações

As importações são deduplicadas e incluídas com base na ordem de sua primeira aparição. Como resultado, a sequência em que você importa seus arquivos desempenha um papel crítico no resultado final agrupado. Embora essas considerações geralmente sejam inconsequentes para projetos pequenos, o empacotamento de projetos maiores pode exigir uma organização adicional. Sempre valide o seu código empacotado.

## Ofuscação

Para uma ofuscação opcional, use `-o`:

```
fry -o sweet mySweetProject.fat  # cria o pacote ofuscado
./sweet                          # executa seu programa da mesma maneira
```

> ao distribuir por meio de hosts públicos, considere [definir uma chave personalizada](../libraries/sdk.md#setkey) com um `.fryrc` local; Apenas o cliente deve ter acesso a esta chave para proteger o fonte

A ofuscação usa o algoritmo [enigma](../libraries/enigma.md) para encriptação, garantindo uma decodificação rápida. Para um tempo de carregamento ótimo, prefira `-b` se a ofuscação não for essencial.
