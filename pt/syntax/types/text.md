{% if output.name != "ebook" %}

# Text

{% endif %}

Textos podem conter muitos caracteres e são às vezes chamados de strings.

## Declaração

Entradas de texto são declaradas usando aspas:

```
a = 'hello world'        # declaração de texto inteligente
a = "hello world"        # declaração de texto bruto
a: Text = 'hello world'  # inteligente, opcionalmente verboso
```

## Manipulando texto

### Concatenação

No FatScript, você pode concatenar, ou juntar, dois textos usando o operador `+`. Essa operação conecta os dois textos em um. Por exemplo:

```
x1 = 'ab' + 'cd'  # Retorna 'abcd'
```

### Subtração de texto

FatScript também suporta uma operação de subtração de texto usando o operador `-`. Essa operação remove uma substring especificada do texto. Por exemplo:

```
x2 = 'ab cd'
x2 - ' ' == 'abcd'  # Retorna true
```

No exemplo acima, o caractere de espaço `' '` é removido do texto original `'ab cd'`, resultando em `'abcd'`.

### Seleção de texto

A seleção permite que você acesse partes específicas de um texto usando índices. No FatScript, você pode usar índices positivos ou negativos. Os índices positivos começam do início do texto (`0` é o primeiro caractere), e os índices negativos começam do final do texto (`-1` é o último caractere).

> para uma explicação detalhada sobre o sistema de indexação no FatScript, consulte a seção sobre acesso e seleção de itens em [List](list.md)

Quando apenas um índice é passado para a função de seleção, um único caractere do texto é selecionado. Quando um intervalo é passados para a função, um fragmento do texto é selecionado. Essa seleção é inclusiva, o que significa que inclui os caracteres nos índices inicial e final, a menos que se use o operador de intervalo semiaberto `..<`, exclusivo no lado direito.

Assim como com as listas, acessar itens que estão fora dos índices válidos irá gerar um erro. Para seleções, não são gerados erros ao acessar índices fora dos limites; em vez disso, um texto vazio é retornado.

```
x3 = 'exemplo'
x3(1)     # 'x'
x3(2, 4)  # 'emp'
x3(..2)   # 'exe'
x3(..<2)  # 'ex'
```

### Manipulação direta

Mutação em textos permite alterar caracteres específicos por posição. Por exemplo, em `~ text = 'ai'`, você pode modificar o último caractere com `text[1] = 'e'`, transformando o texto em `'ae'`.

## Caracteres especiais

Caracteres como aspas `'` / `"` podem ser escapados com a barra invertida `\`.

```
'Rock\'n\'roll'
"Onde fica \"aqui\"?"
```

> você só precisa escapar as aspas do mesmo tipo usadas como delimitador de texto

Outras sequências de escape suportadas são:

- backspace `\b`
- nova linha `\n`
- retorno de carro `\r`
- tabulação `\t`
- escape `\e`
- octeto em representação base-8 `\ooo`
- octeto em representação hexadecimal `\xhh`
- a própria barra invertida `\\`

## Textos inteligentes

Quando declarado com aspas simples `'`, o modo inteligente é habilitado e a interpolação é realizada para qualquer código envolto em chaves `{...}`:

```
texto = 'mundo'
interpolado = 'olá {texto}'  # resulta em 'olá mundo'
```

> o template é processado em uma camada com acesso ao escopo atual

Observe que o uso de novas linhas ou outros textos inteligentes dentro do template de interpolação não é suportado, mas você pode fazer chamadas de método, se precisar compor o resultado com algo mais complexo.

Você pode evitar a interpolação escapando o colchete de abertura:

```
escapado = 'olá \{texto}'  # resulta em 'olá {texto}'
```

Alternativamente, você pode evitar a interpolação usando textos brutos.

## Textos brutos

Quando declarado com aspas duplas `"`, o modo de texto bruto é assumido e a interpolação é desativada.

Exemplo de modo inteligente vs. modo bruto:

```
'Sou inteligente: {interpolado}'  # usando o valor do exemplo anterior
Sou inteligente: olá mundo        # substituição ocorre

"Sou bruto: {interpolado}"  # colchetes são apenas caracteres comuns
Sou bruto: {interpolado}    # nenhuma interpolação ocorre
```

## Operações com textos

- `==` igual
- `!=` diferente
- `+` soma (concatenar)
- `-` subtração (remove substring)
- `<` menor (alfanumérico)
- `<=` menor ou igual (alfanumérico)
- `>` maior (alfanumérico)
- `>=` maior ou igual (alfanumérico)
- `&` AND lógico (convertido para booleano)
- `|` OR lógico (convertido para booleano)

## Codificação

FatScript é projetado para operar com textos codificados em UTF-8. Essa escolha de design reconhece a prevalência desses sistemas de codificação e otimiza a linguagem para ampla compatibilidade.

UTF-8 é um sistema de codificação de vários bytes capaz de representar qualquer caractere no padrão Unicode. Este esquema de codificação de caracteres universais usa de 8 a 32 bits para representar um caractere, permitindo a representação de uma vasta gama de símbolos de diversas línguas e sistemas de escrita. Notavelmente, os primeiros 128 caracteres (0-127) do UTF-8 se alinham precisamente com o conjunto ASCII, tornando qualquer texto ASCII uma string válida codificada em UTF-8.

No FatScript, o tipo de dados Text é uma sequência de caracteres Unicode, inerentemente codificada em UTF-8, portanto, operações como `text.size`, `text(index)` e `text(1..4)` irão contar, acessar ou fatiar corretamente o texto, independentemente da complexidade dos caracteres. Essas operações consideram um caractere UTF-8 multi-byte completo como uma única unidade, garantindo um comportamento correto e previsível.

## Veja também

- [Extensões do protótipo Text](../../libraries/type/text.md)
