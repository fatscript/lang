{% if output.name != "ebook" %}

# Error

{% endif %}

Há grande sabedoria em esperar pelo inesperado também.

## Subtipos padrão

Embora alguns erros possam ser gerados com o tipo base Error, a maioria é [subtipada](index.md#alias-de-tipo).

Veja as definições nas [extensões do protótipo Error](../../libraries/type/error.md).

## Declaração

Erros também podem ser gerados explicitamente; você deve usar o [construtor de tipo](../../libraries/type/error.md):

```
_ <- fat.type.Error

Error('ocorreu um erro')  # gera um erro genérico

MeuErro = Error
MeuErro('ocorreu outro erro')  # gera um erro do subtipo MeuErro
```

## Comparações

Erros sempre avaliam como falso:

```
Error() ? 'é verdadeiro' : 'é falso'  # é falso
```

Erros são comparáveis ​​ao seu tipo:

```
Error() == Error  # verdadeiro
```

> leia também a sintaxe de [comparação de tipo](index.md#checando-tipos)

Uma maneira ingênua de lidar com erros poderia ser:

```
_ <- fat.console
# lidando com o erro retornado
talvezFalhe() <= Error => log('um erro aconteceu')
_                      => log('sucesso')
```

> isso só funciona se a [opção](../../general/options.md) `-e / continuar em caso de erro` estiver definida

Uma outra maneira ingênua de lidar com errors, mas que funciona sempre é utilizar uma [operação padrão](../flow.md#fallback):

```
talvezFalhe() ?? log('um erro aconteceu')
```

Embora a abordagem ingênua possa funcionar, uma maneira mais adequada de lidar com erros é definindo um manipulador de erro usando o método `trapWith` encontrado na [biblioteca failure](../../libraries/failure.md).

## Veja também

- [Biblioteca failure](../../libraries/failure.md)
- [Extensões do protótipo Error](../../libraries/type/error.md)
