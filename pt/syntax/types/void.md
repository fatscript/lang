{% if output.name != "ebook" %}

# Void

{% endif %}

Quando você olha para o 'Vazio', apenas 'nulo' pode ser visto.

## Tem alguém aí fora?

Uma entrada é avaliada como `null` se não estiver definida no escopo atual.

Você pode comparar com `null` usando igualdade `==` ou desigualdade `!=`, como:

```
a == null  # verdadeiro, se 'a' não estiver definida
0 != null  # verdadeiro, porque 0 é um valor definido
```

Tenha em mente que você não pode declarar uma entrada sem valor no FatScript.

Embora você possa atribuir `null` a uma entrada, isso causa comportamentos diferentes, dependendo se a entrada já existe no escopo e se é mutável ou não:

- Se uma entrada ainda não foi declarada, atribuir `null` a ela declara a entrada no escopo, mas a deixa sem um valor observável.
- Se uma entrada foi declarada e é `null`, atribuir `null` a ela não tem efeito.
- Se já existe, é não-nula e imutável, atribuir `null` gera um erro.
- Se já existe, é não-nula e mutável, atribuir `null` remove o valor.

## Declaração de exclusão

Atribuir `null` a uma entrada mutável é o mesmo que excluir seu valor do escopo. Se excluído, seu tipo também é apagado.

```
~ m = 4   # entrada de número mutável
m = null  # exclui m do escopo
```

> entradas nulas são sempre mutáveis e podem transitar para um estado imutável quando um valor é atribuído

## Comparações

Você também pode usar `Void` para verificar o valor de uma entrada, como:

```
()    == Void  # verdadeiro
null  == Void  # verdadeiro
false == Void  # falso
0     == Void  # falso
''    == Void  # falso
[]    == Void  # falso
{}    == Void  # falso
```

Observe que `Void` só aceita `()` e `null`.

## Formas de vazio

Em FatScript, o conceito de "vazio" ou a ausência de um valor pode ser representado de duas maneiras: usando `null` ou parênteses vazios `()`. Eles são efetivamente idênticos, em termos de comportamento no código:

```
null  == null  # verdadeiro
()    == null  # verdadeiro
()    == ()    # verdadeiro
```

### Usando null

A palavra-chave `null` denota explicitamente a ausência de um valor. É comumente usada em cenários onde um parâmetro ou valor de retorno pode não apontar para nenhum valor.

```
method(null, otherParam)

var = null
```

Também pode ser usado para tornar um parâmetro opcional, permitindo que métodos sejam chamados com diferentes números de argumentos:

```
method = (mandatory: Text, optional: Text = null) -> {
  ...
}
```

> `null` pode ser usado explicitamente em qualquer contexto onde uma ausência de valor precisa ser representada

### Usando parênteses vazios

Quando usado no contexto de retornos de métodos, `()` pode significar que o método não retorna nenhum valor significativo.

```
fn = -> {
  doSomething

  ()
}
```

Aqui, `fn` executa alguma ação e então usa `()` para indicar a ausência de um valor de retorno significativo, retornando efetivamente void.

> a diferença reside no estilo de código, então isso é apenas uma sugestão, não uma regra rígida

## Veja também

- [Extensões do protótipo Void](../../libraries/type/void.md)
