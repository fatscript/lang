{% if output.name != "ebook" %}

# Boolean

{% endif %}

Booleanos são muito primitivos, eles só podem ser 'verdadeiro' ou 'falso'.

## Comparação

Além de igualdade `==` e desigualdade `!=`, os booleanos também aceitam os seguintes operadores:

### `&` AND lógico

```
true  & true  == true
true  & false == false
false & true  == false
false & false == false
```

> AND interrompe a expressão se o lado esquerdo for falso

### `|` OR lógico

```
true  | true  == true
true  | false == true
false | true  == true
false | false == false
```

> OR interrompe a expressão se o lado esquerdo for verdadeiro

### `%` XOR lógico (OR exclusivo)

```
true  % true  == false
true  % false == true
false % true  == true
false % false == false
```

> XOR sempre avalia ambos os lados da expressão

## Operador Bang

`!!` converte qualquer tipo em booleano, assim:

- null -> false
- zero (número) -> false
- não-zero (número) -> true
- vazio (texto/lista/escopo/chunk) -> false
- não-vazio (texto/lista/escopo/chunk) -> true
- método -> true
- erro -> false

> AND/OR lógicos (`&`, `|`) e fluxos condicionais (`=>`, `?`) converterão implicitamente para booleano

## Veja também

- [Extensões do protótipo Boolean](../../libraries/type/boolean.md)
- [Tipo Fuzzy](../../libraries/extra/fuzzy.md)
- [Controle de fluxo](../flow.md)
