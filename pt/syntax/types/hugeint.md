{% if output.name != "ebook" %}

# HugeInt

{% endif %}

Um tipo de dado numérico avançado projetado para lidar com inteiros muito grandes.

## Declaração

O tipo `HugeInt` suporta inteiros de até 4096 bits. Veja como você pode declarar um `HugeInt`:

```
h = 0x123456789abcdef  # declaração de um HugeInt
```

> `HugeInt` é sempre expresso em formato hexadecimal

## Operando HugeInts

`HugeInt` suporta uma variedade de operações, tornando-o versátil para cálculos complexos:

- `==` igual
- `!=` diferente
- `+` soma
- `-` subtração
- `*` multiplicação
- `/` divisão
- `%` módulo
- `**` potência
- `<` menor
- `<=` menor ou igual
- `>` maior
- `>=` maior ou igual
- `&` AND lógico
- `|` OR lógico

### Cuidados

No FatScript, `HugeInt` é projetado especificamente como um tipo não sinalizado, e, portanto, só pode representar valores positivos.

Interações entre `HugeInt` e outros tipos numéricos, como [Number](number.md), não estão diretamente disponíveis. Para realizar tais operações, você deve converter o valor para `HugeInt` usando seu construtor (disponível através das extensões de protótipo).

## Precisão

`HugeInt` oferece alta precisão para inteiros muito grandes, essencial em campos como criptografia e computações de grande escala. Esta precisão permanece consistente em toda a sua faixa.

```
prime = 0xfffffffffffffffc90fdA... # um grande número primo
```

Ao contrário dos números de ponto flutuante, `HugeInt` representa valores inteiros discretos, mantendo precisão e espaçamento consistentes em toda a sua faixa:

```
0_____________________________________máx.
|  |  |  |  |  |  |  |  |  |  |  |  |  |  estouro!
```

> o valor máximo é 2^4096 - 1, equivalente a um número com 1233 dígitos decimais ou o literal 0xfff... (com 1024 repetições da letra f)

`HugeInt` é particularmente adequado para cenários que exigem aritmética inteira exata sem erros de arredondamento, especialmente ao lidar com valores muito além dos limites do tipo [Number](number.md). É importante garantir que todas as operações permaneçam dentro de sua capacidade suportada, pois exceder esse limite acarretará em um `ValueError`.

## Veja também

- [Extensões do protótipo HugeInt](../../libraries/type/hugeint.md)
