{% if output.name != "ebook" %}

# Sintaxe

{% endif %}

## Aspectos essenciais

### Importações <-

```
console <- fat.console
```

### Valores (v)

Os nomes dos valores começam com letra minúscula:

```
name = 'Mary'
age = 25
```

> valores são constantes, salvo que declarados inicialmente com til

### Variáveis ~

```
~ email = 'my@email.com'
~ isOnline = true
```

### Listas []

```
list = [ 1, 2, 3 ]

list(0)        # Retorna 1, somente leitura
list[0]        # Retorna 1, leitura/escrita, caso list possa ser alterada
```

### Escopos {}

```
scope = { key1 = 'value1', key2 = 'value2' }

scope.key1     # Retorna 'value1' (acesso por ponto)
scope('key1')  # Retorna 'value1', somente leitura (acesso por chamada)
scope['key1']  # Outputs 'value1', leitura/escrita, caso valor possa ser alterado
```

### Tipos (T)

Os nomes dos tipos começam com letra maiúscula:

```
Person = (name: Text, age: Number)
person = Person('Mary', 25)
```

### Métodos ->

```
greeting = (name: Text): Text -> 'Olá, {name}'
console.log(greeting('Mundo'))
```

> métodos também são considerados valores

### Coalescência nula ??

```
maybeValue ?? fallback    # usa fallback se maybeValue for null/erro
```

### If-Else \_ ? \_ : \_

```
condition ? then : else   # se condition for verdadeira, faz "then", senão "else"
```

### Match de casos =>

```
condition1 => result1
condition2 => result2
conditionN => resultN
_          => default     # caso geral
```

### Switch >>

```
value >> {
  match1 => result1
  match2 => result2
  matchN => resultN
  _      => default       # caso geral
}
```

### Tap <<

```
expression << tapMethod
```

> usa tapMethod apenas pelos seus efeitos sobre valor retornado por expression

### Loops @

```
condition @ loopBody              # loop enquanto condition for verdadeira
1..10 @ n -> rangeMapper(n)       # itera sobre o intervalo de 1 a 10
list @ item -> listMapper(item)   # itera sobre os itens da lista
scope @ key -> scopeMapper(key)   # itera sobre as chaves do escopo
```

### Procedimentos <>

```
~ users = [
  { name = 'Foo', age = 30 }
  { name = 'Bar', age = 28 }
]
userNames = List <> users @ -> _.name
userNames  # Retorna ['Foo', 'Bar']
```

## Exploração detalhada

Nas próximas páginas, você encontrará informações sobre os aspectos centrais da escrita de código FatScript, usando tanto os recursos básicos da linguagem quanto os recursos avançados do sistema de tipos e bibliotecas padrão.

- [Formatação](formatting.md): como formatar código FatScript corretamente

- [Importações](imports.md): como importar bibliotecas no seu código

- [Entradas](entries.md): entendendo o conceito de entradas e escopos

- [Tipos](types/index.md): um guia para o sistema de tipos do FatScript

  - [Any](types/any.md) - qualquer coisa
  - [Void](types/void.md) - nada
  - [Boolean](types/boolean.md) - primitivo
  - [Number](types/number.md) - primitivo
  - [HugeInt](types/hugeint.md) - primitivo
  - [Text](types/text.md) - primitivo
  - [Method](types/method.md) - função ou lambda
  - [List](types/list.md) - como uma matriz ou pilha
  - [Scope](types/scope.md) - como um objeto ou dicionário
  - [Error](types/error.md) - sim, para erros
  - [Chunk](types/chunk.md) - dados binários


- [Controle de fluxo](flow.md): controlando a execução do programa com condicionais

- [Loops](loops.md): como usar intervalos, map-over e loops while
