{% if output.name != "ebook" %}

# Importações

{% endif %}

Vamos desvendar a arte de importar arquivos e bibliotecas no FatScript! Por quê? Bem, porque nesta linguagem você pode importar sempre que seu coração desejar, simplesmente usando uma seta para a esquerda `<-`.

## Sintaxe de ponto

Para usar importações com sintaxe de ponto, os nomes dos arquivos e pastas do projeto não devem começar com dígito nem conter símbolos.

> você pode especificar qualquer caminho que desejar usando [caminhos literais](#caminhos-literais)

### Importação nomeada

Para importar arquivos, use a extensão `.fat` para nomes de arquivo (ou nenhuma extensão). No entanto, omita a extensão na declaração de importação. Aqui está um exemplo:

```
ref <- nomeDoArquivo
```

> se ambos os arquivos `x` e `x.fat` existirem, o último terá precedência

Para importar arquivos de pastas:

```
ref1 <- pasta.nomeDoArquivo
ref2 <- pasta.subPasta.nomeDoArquivo
```

Para importar todos os arquivos de uma pasta, use a sintaxe de "ponto-sublinhado":

```
lib <- pasta._
```

Observe: apenas os arquivos imediatamente dentro da pasta são incluídos usando a sintaxe acima. Para incluir arquivos de subpastas, mencione-os explicitamente. Além disso, um arquivo "\_.fat" (ou arquivo "\_") dentro de uma pasta pode substituir o comportamento de importação de "ponto-sublinhado".

> barras `/` também podem ser usadas como alternativa, como em `ref <- folder/filename`

#### Acesso de elementos

Uma vez importado, acesse os elementos usando a sintaxe de ponto:

```
ref1.elemento1
```

#### Extração de elementos

Para extrair elementos específicos de uma importação nomeada ou para evitar adicionar o nome do módulo todas as vezes (por exemplo, `lib.foo`), use [atribuição por desestruturação](entries.md#atribuição-por-desestruturação):

```
{ foo, bar } = lib
```

#### Visibilidade

Importações nomeadas são resolvidas no escopo global, independentemente de onde forem declaradas. Isso significa que mesmo se você declarar uma importação nomeada dentro de uma função ou escopo local, ela será globalmente acessível.

### Importação local

Para importar no escopo atual, use:

```
_ <- nomeDoArquivo
```

Importações locais, ao contrário das nomeadas, despejam o conteúdo do arquivo diretamente no escopo atual. Assim, um método importado pode ser invocado como `baz(arg)` em vez de `ref.baz(arg)`.

Embora as importações locais sejam bem apropriadas para importar [tipos](types/index.md) no escopo global, elas devem ser usadas com cautela ao importar conteúdos de biblioteca. O uso excessivo de importações locais pode levar a poluição do namespace, tornando mais desafiador seguir o código, porque fica menos aparente de onde vêm os métodos.

## Caminhos literais

Com caminhos literais, você pode usar qualquer nome de arquivo ou extensão. No entanto, observe que essas importações não são avaliadas durante o [empacotamento](../general/bundling.md), mas em tempo de execução. Aqui está um exemplo:

```
ref <- '_pasta/fonte-2.outro'
```

Você também pode usar [textos inteligentes](types/text.md#textos-inteligentes) como caminhos literais:

```
base = 'pasta'
arquivo = 'fonte.xyz'
ref <- '{base}/{arquivo}'
```

Lembre-se de que caminhos literais podem tornar seu código mais complexo e essas importações só podem ser resolvidas dinamicamente, então use-os com moderação.

## Deduplicação de importação

O FatScript utiliza uma estratégia de "importar uma vez" com um mecanismo de sinalizador no escopo, ignorando automaticamente os arquivos que já foram importados.

### Armadilhas do uso de importação

1. **Importações locais dentro do método**: importar diretamente dentro de um corpo de método reavalia a importação em cada invocação, causando retenção de memória:

   ```
   myMethod = -> {
     _ <- lib  # vazamento de memória potencial
     ...
   }
   ```

   > Esse comportamento não é classificado como um bug em si, mas sim uma consequência das escolhas de design no sistema de coleta de lixo (GC) do FatScript. As otimizações do GC excluem nós derivados diretamente do código-fonte, permitindo que eles evitem procedimentos padrão de marcação e varredura. Como resultado, as importações locais dentro dos métodos perdem a a oportunidade de deduplicação, fazendo com que seus nós permaneçam residentes até o fim do programa.

2. **Importações locais seletivas**: Usar atribuição de desestruturação em importações locais descarta outros membros, mas toda a importação é processada e vinculada ao contexto do membro extraído:

   ```
   { foo1 } = { _ <- lib }  # lib é carregada e vinculada ao contexto de foo1
   ...
   { foo2 } = { _ <- lib }  # lib é carregada novamente e vinculada a foo2
   ```

   > Este padrão cria um fechamento. Usá-lo para a mesma biblioteca resulta em cargas repetidas, aumentando o uso de memória. Para melhor eficiência, considere importar a biblioteca uma vez no nível superior e referenciá-la diretamente ou usar importações seletivas com moderação.

### Melhores práticas

Para evitar problemas de memória, siga estas estratégias:

- **Mover importações para escopo externo**: Importar bibliotecas em um nível mais alto para garantir avaliação única.
- **Usar importações nomeadas**: Prefira importações nomeadas para reutilizar código sem redundância.
